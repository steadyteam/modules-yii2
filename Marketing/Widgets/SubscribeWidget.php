<?php

namespace Steady\Modules\Marketing\Widgets;

use Steady\Engine\SW;
use yii\base\Widget;

class SubscribeWidget extends Widget
{
    const COOKIE_KEY = 'sw_is_subscriber';

    /**
     * @var string
     */
    public $mainView;

    /**
     * @var bool
     */
    public $isSubscriber;

    /**
     * @throws \Exception
     */
    public function init()
    {
        parent::init();

        $this->isSubscriber = SW::$app->request->cookies->get(self::COOKIE_KEY) ? true : false;
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render($this->mainView, [
            'isSubscriber' => $this->isSubscriber,
        ]);
    }
}