<?php

namespace Steady\Modules\Marketing\Forms;

use Steady\Engine\Base\Form;
use Steady\Engine\SW;

class SubscribeForm extends Form
{
    /**
     * @var string
     */
    public $email;

    public function rules()
    {
        return [
            ['email', 'email'],
            ['email', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => SW::t('admin', 'Email'),
        ];
    }
}