<?php

namespace Steady\Modules\Email;

use Steady\Engine\Base\Module;

class EmailModule extends Module
{
    public static $installConfig = [
        'name' => 'email',
        'class' => self::class,
        'title' => [
            'en' => 'Emails',
            'ru' => 'Емейлы',
        ],
        'icon' => 'envelope',
        'order_num' => 45,
    ];

    public $settings = [
    ];

}