<?php

namespace Steady\Modules\Email\Api;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\Components\TreeObjectInterface;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Modules\Email\Models\EmailModel;

/**
 * @property EmailModel $model
 */
class EmailObject extends ApiObject implements TreeObjectInterface
{
    use TreeObjectTrait;

    public $modelName = EmailModel::class;

    public $email_id;
    public $from;
    public $from_name;
    public $to;
    public $subject;
    public $body;
}