<?php

namespace Steady\Modules\Email\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\Components\TreeApiTrait;
use Steady\Modules\Email\EmailModule;
use Steady\Modules\Email\Models\EmailModel;

class EmailApi extends Api
{
    use TreeApiTrait;

    public $moduleName = EmailModule::class;

    public $modelName = EmailModel::class;

    public $objectName = EmailObject::class;
}