<?php

namespace Steady\Modules\Email\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\TreeModel;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int email_id
 * @property string from
 * @property string from_name
 * @property string to
 * @property string subject
 * @property string body
 * @property int result
 *
 * @mixin SluggableBehavior
 */
class EmailModel extends TreeModel
{
    public function behaviors()
    {
        $array = [
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();
    }

    public static function tableName()
    {
        return 'emails';
    }

    public function rules()
    {
        $array = [
            ['from', 'required'],
            ['from', 'string'],
            ['from_name', 'string'],
            ['to', 'required'],
            ['to', 'string'],
            ['subject', 'required'],
            ['subject', 'string'],
            ['body', 'required'],
            ['body', 'string'],
            ['result', 'integer'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'email_id' => $migration->primaryKey(),
            'from' => $migration->string(256),
            'from_name' => $migration->string(256),
            'to' => $migration->text(),
            'subject' => $migration->string(256),
            'body' => $migration->text(),
            'result' => $migration->integer(3),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new EmailModel();
        $model->from = 'base';
        $model->from_name = 'Base';
        $model->to = 'base';
        $model->subject = 'Base';
        $model->body = 'Base';
        $model->type = 0;
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }
}