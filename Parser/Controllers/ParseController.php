<?php

namespace Steady\Modules\Parser\Controllers;

use GuzzleHttp\Client;
use phpQuery as PhpQuery;
use Steady\Admin\Components\AdminController;
use Steady\Engine\Models\PhotoModel;
use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\AttributeModel;
use Steady\Modules\Catalog\Models\BrandModel;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use Steady\Modules\Catalog\Models\OfferModel;
use vipnytt\SitemapParser;
use yii\helpers\Inflector;
use yii\helpers\Url;

class ParseController extends AdminController
{
    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionGoods()
    {
        ini_set('max_execution_time', 900);

        $client = new Client();

        $sitemapUrl = Url::to('public/static/parser/sitemap_iblock_6.xml', true);

        $result = '';
        $parsedGoodsCount = 0;

        //AttributeModel::insertRoot();

        $transaction = $transaction = SW::$app->db->beginTransaction();
        try {
            $parser = new SitemapParser('MyCustomUserAgent', ['strict' => false]);
            $parser->parse($sitemapUrl);
            $i = SettingModel::get('parser_goods_count') ?? 0;
            $j = 0;
            foreach ($parser->getURLs() as $url => $tags) {
                $j++;
                if ($i >= $j) continue;
                $i++;
                $url = $this->handleUrl($url, true);

                $slug = explode('/', $url);
                end($slug);
                $slug = prev($slug);

                if (GoodsModel::find()->where(['slug' => $slug])->one()) {
                    continue;
                }

                try {
                    $res = $client->request('GET', $url);
                } catch (\Exception $e) {
                    continue;
                }
                $body = $res->getBody();
                $document = PhpQuery::newDocumentHTML($body);

                $title = trim(pq($document->find('div.catalogTitleBlock h1'))->text());
                $description = trim(pq($document->find('div.product_card_description'))->text());
                $category = pq($document->find('div.breadcrumb a[itemprop]'))->texts();
                $category = trim(end($category));
                /** @var CategoryModel $category */
                $category = CategoryModel::find()->where(['title' => $category])->one();
                $brand = trim(pq($document->find('div.brandText a'))->text());
                $brandId = BrandModel::find()
                    ->select('brand_id')->where(['title' => $brand])->scalar();
                $vendorCode = trim(str_replace('Артикул:', '',
                    pq($document->find('div.productArticle'))->text()
                ));
                $offersElements = $document->find('span.offer_option_text');
                $offers = [];
                foreach ($offersElements as $el) {
                    $span = pq($el);
                    $offers[] = $span->text();
                }
                $pricesElements = $document->find('span.actual_price');
                $prices = [];
                foreach ($pricesElements as $el) {
                    $span = pq($el);
                    $prices[] = trim(str_replace(array('c ', ' '), '', $span->text()));
                }
                $imagesElements = $document->find('div#previews_slider_wrapper a');
                $images = [];
                foreach ($imagesElements as $el) {
                    $a = pq($el);
                    $images[] = $a->attr('data-preview-image');
                }
                $attrsElements = $document->find('div.characteristics_block');
                $attrs = [];
                foreach ($attrsElements as $el) {
                    $attr = pq($el);
                    $attrs[] = trim($attr->text());
                }
                /*$associatedGoodsElements = $document->find('div.productCarousel a.productName');
                $associatedGoods = [];
                foreach ($associatedGoodsElements as $el) {
                    $associatedGood = pq($el);
                    $associatedGoods[] = trim($associatedGood->text());
                }*/

                /*if (!$prices) continue;*/

                if ($title && $category) {
                    $goods = new GoodsModel();
                    $goods->category_id = $category->category_id;
                    $goods->brand_id = (int)$brandId;
                    $goods->title = $title;
                    $goods->slug = $slug;
                    $goods->description = $description;
                    $goods->saveEx();

                    if ($vendorCode) {
                        $goods->fields = ['vendor-code' => $vendorCode];
                    }
                    $goods->saveEx();

                    if ($offers) {
                        for ($k = 0; $k < count($offers); $k++) {
                            $offer = new OfferModel();
                            $offer->goods_id = $goods->goods_id;
                            $offer->slug = Inflector::slug(Inflector::transliterate($offers[$k]));
                            $offer->title = $offers[$k];
                            $offer->price = (float)$prices[$k];
                            $offer->saveEx();
                        }
                    }

                    $im = 0;
                    foreach ($images as $image) {
                        $imageUrl = 'http://amatagroup.ru' . $image;
                        $imagePath = "parser/images/goods/goods-$goods->goods_id-$im.jpg";
                        $imageFile = SW::getAlias("@uploads/$imagePath");
                        $image = STD_URL_UPLOADS . "/$imagePath";

                        $imageCopy = $this->copyRemote($imageUrl, $imageFile);

                        if ($imageCopy) {
                            if ($im == 0) {
                                $goods->image = $image;
                                $goods->saveEx();
                            } else {
                                $photo = new PhotoModel();
                                $photo->owner_id = $goods->goods_id;
                                $photo->class_alias = GoodsModel::getClassAlias();
                                $photo->image = $image;
                                $photo->saveEx();
                            }
                            $im++;
                        }
                    }

                    foreach ($attrs as $attr) {
                        $data = explode(':', $attr);
                        $title = trim($data[0]);
                        $content = trim($data[1]);
                        $alias = Inflector::slug(Inflector::transliterate($title));
                        $category->addAttr($alias, $title, AttributeModel::TYPE_STRING);
                        $goods->setAttr($alias, $content);
                    }

                    $parsedGoodsCount++;

                    SettingModel::setEx('parser_goods_count', $i, 'Parser Goods Count',
                        SettingModel::VISIBLE_NONE);
                }

                if ($parsedGoodsCount == 100) break;
            }
            $transaction->commit();
            return "Success! Parsed $parsedGoodsCount Goods<br>Count: $i<br>Items: $j<br>$result";
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \yii\db\Exception
     */
    public function actionCategories()
    {
        $client = new Client();

        $parsedCategoriesCount = 0;

        $transaction = $transaction = SW::$app->db->beginTransaction();
        try {
            $url = 'https://www.amatagroup.ru/';
            $res = $client->request('GET', $url);
            $body = $res->getBody();
            $document = PhpQuery::newDocumentHTML($body);

            $categoriesElements = $document->find('div.mainLeftMenu li');
            CategoryModel::insertRoot();
            $i = 0;
            $j = 0;
            $k = 0;
            foreach ($categoriesElements as $el) {
                $li = pq($el);
                $name = trim($li->find('> a')->text());
                $slug = explode('/', trim($li->find('> a')->attr('href')));
                end($slug);
                $slug = prev($slug);
                $category = new CategoryModel();
                $category->title = $name;
                $category->slug = $slug;
                if ($li->hasClass('topLvlLi')) {
                    $category->parent_id = 1;
                    $category->saveEx();
                    $i = $category->category_id;
                } else if ($li->hasClass('firstLvlLi')) {
                    $category->parent_id = $i;
                    $category->saveEx();
                    $j = $category->category_id;
                } else {
                    $category->parent_id = $j;
                    $category->saveEx();
                }
                $parsedCategoriesCount++;
            }

            $transaction->commit();
            return "Success! Parsed $parsedCategoriesCount Categories<br>";
        } catch (\Exception $e) {
            $transaction->rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \yii\db\Exception
     */
    public function actionBrands()
    {
        $client = new Client();

        $parsedBrandsCount = 0;

        $transaction = $transaction = SW::$app->db->beginTransaction();
        try {
            $url = 'https://www.amatagroup.ru/brands/';
            $res = $client->request('GET', $url);
            $body = $res->getBody();
            $document = PhpQuery::newDocumentHTML($body);

            $brandsElements = $document->find('div.brandPhotoWrapper a');
            BrandModel::insertRoot();

            foreach ($brandsElements as $el) {
                $a = pq($el);
                $slug = explode('/', $a->attr('href'))[2];
                if ($parsedBrandsCount == 115) {
                    $slug = 'toys';
                }
                $img = $a->find('img');
                if ($img->length) {
                    $title = $img->attr('title');
                    $image = $img->attr('src');

                    $imageUrl = ('https://www.amatagroup.ru' . $image);
                    $imagePath = "parser/images/brands/$slug.jpg";
                    $imageFile = SW::getAlias("@uploads/$imagePath");
                    $image = STD_URL_UPLOADS . "/$imagePath";

                    $this->copyRemote($imageUrl, $imageFile);
                } else {
                    $title = $a->text();
                    $image = null;
                }

                $brand = new BrandModel();
                $brand->title = $title;
                $brand->slug = $slug;
                $brand->image = $image;
                $brand->parent_id = 1;
                $brand->saveEx();

                $parsedBrandsCount++;
            }

            $transaction->commit();
            return "Success! Parsed $parsedBrandsCount Brands<br>";
        } catch (\Exception $e) {
            $transaction->rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @return string
     * @throws SitemapParser\Exceptions\SitemapParserException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionGoodsData()
    {
        ini_set('max_execution_time', 900);

        $client = new Client();

        $sitemapUrl = Url::to('public/static/parser/sitemap_iblock_5.xml', true);

        $result = '';
        $parsedGoodsCount = 0;

        $transaction = $transaction = SW::$app->db->beginTransaction();
        try {
            $parser = new SitemapParser('MyCustomUserAgent', ['strict' => false]);
            $parser->parse($sitemapUrl);
            $i = SettingModel::get('parser_goods_count') ?? 0;
            $j = 0;
            foreach ($parser->getURLs() as $url => $tags) {
                $j++;
                if ($i >= $j) continue;
                $i++;
                $url = $this->handleUrl($url, true);

                $slug = explode('/', $url);
                end($slug);
                $slug = prev($slug);

                /** @var GoodsModel $goods */
                $goods = GoodsModel::find()->where(['slug' => $slug])->one();

                if (!$goods) {
                    continue;
                }

                try {
                    $res = $client->request('GET', $url);
                } catch (\Exception $e) {
                    continue;
                }
                $body = $res->getBody();
                $document = PhpQuery::newDocumentHTML($body);

                $imagesElements = $document->find('div#previews_slider_wrapper a');
                $images = [];
                foreach ($imagesElements as $el) {
                    $a = pq($el);
                    $images[] = $a->attr('data-preview-image');
                }
                $associatedGoodsElements = $document->find('div.productCarousel a.productName');
                $associatedGoods = [];
                foreach ($associatedGoodsElements as $el) {
                    $associatedGood = pq($el);
                    $associatedGoods[] = trim($associatedGood->text());
                }

                if ($images) {
                    $im = 0;
                    foreach ($images as $image) {
                        $imageUrl = 'http://amatagroup.ru' . $image;
                        $imagePath = "parser/images/goods/goods-$goods->goods_id-$im.jpg";
                        $imageFile = SW::getAlias("@uploads/$imagePath");
                        $image = STD_URL_UPLOADS . "/$imagePath";

                        $imageCopy = $this->copyRemote($imageUrl, $imageFile);

                        if ($imageCopy) {
                            if ($im == 0) {
                                $goods->image = $image;
                                $goods->saveEx();
                            } else {
                                $photo = new PhotoModel();
                                $photo->owner_id = $goods->goods_id;
                                $photo->class_alias = GoodsModel::getClassAlias();
                                $photo->image = $image;
                                $photo->saveEx();
                            }
                            $im++;
                        }
                    }
                }

                $parsedGoodsCount++;

                SettingModel::setEx('parser_goods_count', $i, 'Parser Goods Count',
                    SettingModel::VISIBLE_NONE);

                if ($parsedGoodsCount == 200) break;
            }
            $transaction->commit();
            return "Success! Parsed $parsedGoodsCount Goods<br>Count: $i<br>Items: $j<br>$result";
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @name $url
     * @name bool $http
     * @return string
     */
    protected function handleUrl($url, $http = false)
    {
        $url = rtrim(urldecode($url));
        if ($http && $url[0] != 'h') {
            $url = strstr($url, 'h');
        }

        return $url;
    }

    /**
     * @name $fromUrl
     * @name $toFile
     * @return bool|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function copyRemote($fromUrl, $toFile)
    {
        try {
            $client = new Client();
            $result = $client->request('GET', $fromUrl, ['sink' => $toFile]);
        } catch (\Exception $e) {
            return false;
        }

        return $result;
    }
}