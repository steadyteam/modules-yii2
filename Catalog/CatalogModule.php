<?php

namespace Steady\Modules\Catalog;

use Steady\Admin\Components\SteadyModule;

class CatalogModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'catalog',
        'class' => self::class,
        'title' => [
            'en' => 'Catalog',
            'ru' => 'Каталог',
        ],
        'icon' => 'list-alt',
        'order_num' => 75,
    ];
    public $settings = [
        'categoryThumb' => true,
        'itemsInFolder' => true,

        'itemThumb' => true,
        'itemPhotos' => true,
        'itemDescription' => true,
        'itemSale' => true,

        'watermark' => '@frontend/img/watermark.png',
    ];
}