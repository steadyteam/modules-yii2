<?php

namespace Steady\Modules\Catalog\Interfaces;

interface Goodsable
{
    public function goods();

    public function goodsCount();
}