<?php

namespace Steady\Modules\Catalog\Interfaces;

interface Attributable
{
    public function attrs();
}