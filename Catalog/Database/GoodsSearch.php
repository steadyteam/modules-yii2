<?php

namespace Steady\Modules\Catalog\Database;

use Steady\Engine\Base\Query;
use Steady\Engine\Models\TagModel;
use Steady\Engine\Modules\Field\Models\FieldAssignModel;
use Steady\Modules\Catalog\Models\AttributesAssignModel;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;

class GoodsSearch
{
    /**
     * @var Query
     */
    protected $query;

    /**
     * @var string
     */
    protected $goodsTable;

    /**
     * @var bool
     */
    protected $skipEmpty;

    /**
     * @var CategoryModel[]
     */
    protected $categories;

    /**
     * @name bool $skipEmpty
     * @name Query|null $query
     * @name bool $group
     */
    public function __construct($skipEmpty = false, Query $query = null, $group = true)
    {
        $this->skipEmpty = $skipEmpty;

        if (!$query) {
            $this->query = GoodsModel::find();
        } else {
            $this->query = $query;
        }

        $this->goodsTable = GoodsModel::tableName();

        if ($group) {
            $this->group(["$this->goodsTable.goods_id"]);
        }
    }

    /**
     * @return Query
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @name array $categoryIds
     * @name null $depth
     * @return GoodsSearch
     */
    public function category($categoryIds, $depth = null)
    {
        $categoryIds = $this->validate($categoryIds, true);
        if (!$categoryIds) return $this;

        $this->categories = CategoryModel::find()->where(['category_id' => $categoryIds])->all();

        foreach ($this->categories as $category) {
            $categoryIds = array_merge($categoryIds, $category->getDescendantsIds($depth, true));
        }

        //print_r($categoryIds); exit();

        $this->query->andWhere(["$this->goodsTable.category_id" => $categoryIds]);

        return $this;
    }

    /**
     * @name array $brandIds
     * @return GoodsSearch
     */
    public function brand($brandIds)
    {
        $brandIds = $this->validate($brandIds, true);
        if (!$brandIds) {
            return $this;
        }

        $this->query->andWhere(["$this->goodsTable.brand_id" => $brandIds]);

        return $this;
    }

    /**
     * @param $attrs
     * @param bool $likeWhere
     * @return $this
     */
    public function attributes($attrs, $likeWhere = false)
    {
        $attrs = $this->validate($attrs, true);
        if (!$attrs) return $this;

        // TODO: Возможно, стоит делать отдельный запрос в таблицу, проверять каждое поле
        $this->query->innerJoinWith('attributesRelation');

        foreach ($attrs as $idx => $attr) {
            if (!$likeWhere) {
                $this->query->andWhere([
                    AttributesAssignModel::tableName() . '.attribute_id' => $idx,
                    AttributesAssignModel::tableName() . '.content' => $attr,
                ]);
            } else {
                $this->query->andWhere([
                    'like', AttributesAssignModel::tableName() . '.content', $attr,
                ]);
            }
        }

        return $this;
    }

    /**
     * @param $tagsNames
     * @return $this
     */
    public function tags($tagsNames)
    {
        $tagsNames = $this->validate($tagsNames, true);
        if (!$tagsNames) return $this;

        $this->query->innerJoinWith('tags')
            ->andWhere([TagModel::tableName() . '.name' => $tagsNames]);

        return $this;
    }

    /**
     * @param int $minPrice
     * @param int|null $maxPrice
     * @return GoodsSearch
     */
    public function price($minPrice = 0, $maxPrice = null)
    {
        if ($minPrice > 0) {
            $this->query->andWhere(['>', "$this->goodsTable.price", intval($minPrice)]);
        }
        if ($maxPrice > 0) {
            $this->query->andWhere(['<', "$this->goodsTable.price", intval($maxPrice)]);
        }

        return $this;
    }

    /**
     * @param bool|null $value
     * @return $this
     */
    public function available(bool $value = null)
    {
        if ($value !== null) {
            $this->query->andWhere(["$this->goodsTable.available" => (bool)$value]);
            $this->query->andWhere(['>', "$this->goodsTable.price", 0]);
        }

        return $this;
    }

    /**
     * @name $text
     * @return $this
     */
    public function title(string $text)
    {
        $this->query->andWhere(['like', GoodsModel::tableName() . '.title', $text]);

        return $this;
    }

    /**
     * @name $content
     * @return $this
     */
    public function withFieldsContent(string $content)
    {
        $this->query->innerJoinWith('fieldsRelation')
            ->orWhere(['like', FieldAssignModel::tableName() . '.content', $content]);

        return $this;
    }

    /**
     * @param array $columns
     * @return $this
     */
    public function group(array $columns)
    {
        $this->query->groupBy($columns);

        return $this;
    }

    /**
     * @param array $sorts
     * @return $this
     */
    public function sort(array $sorts)
    {
        if ($sorts && count($sorts)) {
            $columns = [];
            foreach ($sorts as $idx => $value) {
                if ($idx === 'name') {
                    $field = "$this->goodsTable.title";
                } else if ($idx === 'price') {
                    $field = "$this->goodsTable.price";
                } else {
                    $field = null;
                }
                if ($field) {
                    $columns = ArrayHelper::merge($columns, [$field => $value === 'asc' ? SORT_ASC : SORT_DESC]);
                }
            }
            if (count($columns)) {
                $this->query->orderBy($columns);
            }
        }

        return $this;
    }

    /**
     * @name $value
     * @name bool $needArray
     * @return mixed
     */
    protected function validate($value, $needArray = true)
    {
        if ($needArray) {
            if (!is_array($value)) $value = array($value);
            $value = array_filter($value);
        }

        if (!$value) {
            if ($this->skipEmpty) return false;
            else throw new InvalidArgumentException();
        }

        return $value;
    }
}