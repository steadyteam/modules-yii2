<?php

namespace Steady\Modules\Catalog\Database;

use Steady\Engine\Base\Model;
use Steady\Modules\Catalog\Models\BrandModel;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use Steady\Modules\Catalog\Models\OfferModel;

class CatalogDataBase
{
    /**
     * @name array $categoriesIds
     * @return BrandModel[]|null
     */
    public static function getBrandsByCategories(array $categoriesIds)
    {
        $brandsIds = self::getBrandsIdsByCategoriesIds($categoriesIds);

        //print_r($categoriesIds); exit();

        return BrandModel::find()
            ->where(['brand_id' => $brandsIds])
            ->all();
    }

    /**
     * @name array $brandIds
     * @return array
     */
    public static function getGoodsIdsByBrandsIds(array $brandIds)
    {
        return GoodsModel::find()
            ->select(['goods_id'])
            ->where(['brand_id' => $brandIds])
            ->asArray()
            ->column();
    }

    /**
     * @name array $brandIds
     * @return array
     */
    public static function getCategoriesIdsByBrandsIds(array $brandIds)
    {
        return GoodsModel::find()
            ->select(['category_id'])
            ->distinct()
            ->where(['brand_id' => $brandIds])
            ->asArray()
            ->column();
    }

    /**
     * @name array $categoriesIds
     * @return array
     */
    public static function getBrandsIdsByCategoriesIds(array $categoriesIds)
    {
        return GoodsModel::find()
            ->select(['brand_id'])
            ->distinct()
            ->where(['category_id' => $categoriesIds])
            ->asArray()
            ->column();
    }

    /**
     * @name Model $model
     * @return array
     */
    public static function getPriceRangeByModel(Model $model)
    {
        $where = [];
        if ($model instanceof CategoryModel) {
            $where = ['g.category_id' => $model->category_id];
        } else if ($model instanceof BrandModel) {
            $where = ['g.brand_id' => $model->brand_id];
        }

        return GoodsModel::find()
            ->from(['g' => GoodsModel::tableName()])
            ->select(['MAX(coalesce(o.price, 0)) AS max_price', 'MIN(coalesce(o.price, 0)) AS min_price'])
            ->innerJoin(OfferModel::tableName() . ' o', 'o.goods_id = g.goods_id and o.price > 0')
            ->where($where)
            ->asArray()
            ->one();
    }
}