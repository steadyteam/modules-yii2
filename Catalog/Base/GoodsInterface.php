<?php

namespace Steady\Modules\Catalog\Base;

interface GoodsInterface
{
    public function getId();

    public function getUniqueId();

    public function getPrice();

    public static function loadItem($id): ?GoodsInterface;
}