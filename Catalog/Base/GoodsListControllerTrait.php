<?php

namespace Steady\Modules\Catalog\Base;

use Steady\Engine\SW;

trait GoodsListControllerTrait
{
    public function actionFavourites()
    {
        $favourites = SW::$app->api->catalog->favourites;

        return $this->render('catalog/favourites', [
            'favourites' => $favourites,
            'goodsArray' => $favourites->fetch(),
        ]);
    }

    public function getGoodsList($listName)
    {
        $goodsList = SW::$app->api->catalog->getGoodsList($listName);

        return $this->render('catalog/goods_list', [
            'goodsList' => $goodsList,
        ]);
    }
}