<?php

namespace Steady\Modules\Catalog\Base;

use yii\base\InvalidArgumentException;

class GoodsList implements GoodsListInterface
{
    /**
     * @var StorageInterface
     */
    public $storage;

    /**
     * @var integer|null
     */
    public $sort = null;

    /**
     * @var GoodsItem[]
     */
    protected $items = [];

    /**
     * GoodsList constructor
     * @name StorageInterface $storage
     * @name integer|null $sort
     */
    public function __construct(StorageInterface $storage, ?int $sort = null)
    {
        $this->storage = $storage;
        $this->sort = $sort;

        $this->storage->load($this);
    }

    /**
     * @name GoodsInterface $item
     * @name int $quantity
     * @name bool $priority
     * @name bool $save
     * @return $this
     */
    public function put(GoodsInterface $item, int $quantity = 1, $priority = true, $save = true)
    {
        $uniqueId = $item->getUniqueId();

        if (!isset($this->items[$uniqueId]) || !$this->items[$uniqueId] instanceof GoodsItem) {
            $this->items[$uniqueId] = new GoodsItem($item->getId(), 0, $item);
        }

        $currentQuantity = $this->items[$uniqueId]->quantity ?? 0;

        $this->items[$uniqueId]->quantity = $currentQuantity + $quantity;

        $this->sort($uniqueId, $priority);

        if ($save) {
            $this->storage->save($this);
        }

        return $this;
    }

    /**
     * @name $uniqueId
     * @name int $quantity
     * @name bool $put
     * @name bool $save
     * @return $this
     */
    public function plus($uniqueId, int $quantity = 1, $put = false, $save = true)
    {
        $currentQuantity = $this->items[$uniqueId]->quantity ?? 0;

        if ($currentQuantity > 0) {
            $this->items[$uniqueId]->quantity = $currentQuantity + $quantity;
        } else if ($put) {
            // TODO: Реализовать получение объекта товара, для добавления
            //$this->put($uniqueId, $save);
        }

        //$this->sort($uniqueId);

        if ($save) {
            $this->storage->save($this);
        }

        return $this;
    }

    /**
     * @name $uniqueId
     * @name int $quantity
     * @name bool $remove
     * @name bool $save
     * @return $this
     */
    public function minus($uniqueId, int $quantity = 1, $remove = false, $save = true)
    {
        $currentQuantity = $this->items[$uniqueId]->quantity ?? 0;

        if ($currentQuantity > 1) {
            $this->items[$uniqueId]->quantity = $currentQuantity - $quantity;
        } else if ($remove) {
            $this->remove($uniqueId, $save);
        }

        //$this->sort($uniqueId);

        if ($save) {
            $this->storage->save($this);
        }

        return $this;
    }

    /**
     * @name $uniqueId
     * @return null|GoodsItem
     */
    public function get($uniqueId): ?GoodsItem
    {
        if (!isset($this->items[$uniqueId])) {
            return null;
        }

        return $this->items[$uniqueId];
    }

    public function set($uniqueId)
    {
        // TODO: Implement set() method.
    }

    /**
     * @name $uniqueId
     * @name bool $save
     * @return $this
     */
    public function remove($uniqueId, $save = true)
    {
        if (!isset($this->items[$uniqueId])) {
            throw new InvalidArgumentException('Item not found');
        }

        unset($this->items[$uniqueId]);

        if ($save) {
            $this->storage->save($this);
        }

        return $this;
    }

    /**
     * @name $uniqueId
     * @return int
     */
    public function quantity($uniqueId)
    {
        if (!isset($this->items[$uniqueId])) {
            throw new InvalidArgumentException('Item quantity not found');
        }

        return $this->items[$uniqueId]->quantity;
    }

    /**
     * @name $uniqueId
     * @name bool $priority
     * @return $this
     */
    public function sort($uniqueId, $priority = true)
    {
        if (!isset($this->items[$uniqueId])) {
            throw new InvalidArgumentException('Item not found');
        }

        if ($priority && $this->sort !== null) {
            $this->items[$uniqueId]->priority = time() + count($this->items);
        } else {
            $this->items[$uniqueId]->priority = 0;
        }

        return $this;
    }

    /**
     * @name $uniqueId
     * @return float|int
     */
    public function price($uniqueId)
    {
        if (!isset($this->items[$uniqueId])) {
            throw new InvalidArgumentException('Item not found');
        }

        return $this->items[$uniqueId]->object->getPrice() * $this->items[$uniqueId]->quantity;
    }

    /**
     * @return bool
     */
    public function empty()
    {
        return empty($this->items);
    }

    /**
     * @name bool $save
     * @return $this
     */
    public function clear($save = true)
    {
        $this->items = [];

        if ($save) {
            $this->storage->save($this);
        }

        return $this;
    }

    /**
     * @name integer|null $limit
     * @return GoodsItem[]|array
     */
    public function fetch(?int $limit = null)
    {
        if ($this->sort === null) {
            $items = $this->items;
        } else {
            $priorities = [];
            foreach ($this->items as $item) {
                $priorities[$item->object->getUniqueId()] = $item->priority;
            }
            if ($this->sort === SORT_ASC) {
                asort($priorities);
            } else if ($this->sort === SORT_DESC) {
                arsort($priorities);
            }

            $items = [];
            foreach ($priorities as $key => $value) {
                $items[$key] = $this->items[$key];
            }
        }
        if ($limit !== null && is_numeric($limit)) {
            $items = array_slice($items, 0, $limit);
        }
        return $items;
    }

    /**
     * @return GoodsItem[]
     */
    public function fetchData()
    {
        return $this->items;
    }

    /**
     * @name bool $different
     * @return float|int
     */
    public function count(bool $different = true)
    {
        if (!$different) {
            $count = count($this->items);
        } else {
            $count = 0;
            if (!empty($this->items)) {
                foreach ($this->items as $item) {
                    $count += $item->quantity;
                }
            }
        }

        return $count;
    }

    /**
     * @throws \Exception
     */
    public function amount()
    {
        $amount = 0;

        if (!empty($this->items)) {
            foreach ($this->items as $item) {
                $amount += $item->object->getPrice() * $item->quantity;
            }
        }

        return $amount;
    }
}