<?php

namespace Steady\Modules\Catalog\Base;

use Steady\Modules\Catalog\Api\GoodsObject;

/**
 * @mixin GoodsObject
 */
class GoodsItem
{
    /**
     * @var int
     */
    public $goodsId;

    /**
     * @var int
     */
    public $quantity;

    /**
     * @var int
     */
    public $priority;

    /**
     * @var GoodsInterface
     */
    public $object;

    /**
     * @var float
     */
    protected $sum;

    /**
     * GoodsItem constructor.
     * @param int $goodsId
     * @param int $quantity
     * @param GoodsInterface $object
     * @param null $sum
     */
    public function __construct(int $goodsId, int $quantity, GoodsInterface $object, $sum = null)
    {
        $this->goodsId = $goodsId;
        $this->quantity = $quantity;
        $this->object = $object;
        if (!$sum) {
            $this->calculateSum();
        } else {
            $this->sum = $sum;
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        // TODO: Проверить работу, возможна путаница при вызове title и title(), twig вызывает метод, а не свойство
        return $this->object->$name;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        // TODO: Проверить работу, возможна путаница при вызове title и title(), twig вызывает метод, а не свойство
        if (method_exists($this->object, $name)) {
            return call_user_func_array([$this->object, $name], $arguments);
        } else {
            return $this->object->$name;
        }
    }

    /**
     * TODO: В твиге работает как setter, стоит разобраться
     * @return float|int
     */
    public function getSum()
    {
        if (!$this->sum) {
            $this->calculateSum();
        }
        return $this->sum;
    }

    /**
     * @return float|int
     */
    public function calculateSum()
    {
        $this->sum = $this->quantity * $this->object->getPrice();

        return $this->sum;
    }
}