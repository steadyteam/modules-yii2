<?php

namespace Steady\Modules\Catalog\Base;

interface GoodsListInterface
{
    public function put(GoodsInterface $item, int $quantity = 1, $priority = true, $save = true);

    public function plus($uniqueId);

    public function minus($uniqueId);

    public function get($uniqueId): ?GoodsItem;

    public function set($uniqueId);

    public function remove($uniqueId);

    public function quantity($uniqueId);

    public function sort($uniqueId);

    public function price($uniqueId);

    public function empty();

    public function clear();

    public function fetch(?int $limit = null);

    public function count(bool $different = false);

    public function amount();
}