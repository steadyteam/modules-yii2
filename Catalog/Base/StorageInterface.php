<?php

namespace Steady\Modules\Catalog\Base;

interface StorageInterface
{
    public function save(GoodsListInterface $goodsList);

    public function load(GoodsListInterface $goodsList);

    public function synch(GoodsListInterface $goodsList);

    public function clear();
}