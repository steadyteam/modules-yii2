<?php

namespace Steady\Modules\Catalog\Components;

use Steady\Engine\Base\Controller;
use Steady\Engine\SW;
use yii\web\Response;

class CatalogActionController extends Controller
{
    public function actionAddToList($id, $listName)
    {
        $item = SW::$app->api->catalog->category->item($id);
        $goodsList = SW::$app->api->catalog->getGoodsList($listName);

        $goodsList->put($item);

        if (SW::$app->request->isAjax) {
            SW::$app->response->format = Response::FORMAT_JSON;
            return [
                'success' => true,
            ];
        }

        return $this->redirect(["/$listName"]);
    }

    public function actionRemoveFromList($id, $listName)
    {
        $item = SW::$app->api->catalog->category->item($id);
        $goodsList = SW::$app->api->catalog->getGoodsList($listName);

        $goodsList->remove($item->getUniqueId());

        if (SW::$app->request->isAjax) {
            SW::$app->response->format = Response::FORMAT_JSON;
            return [
                'success' => true,
            ];
        }

        return $this->redirect(["/$listName"]);
    }

    public function actionPlus($id, $listName)
    {
        $item = SW::$app->api->catalog->category->item($id);
        $goodsList = SW::$app->api->catalog->getGoodsList($listName);

        $goodsList->put($item);

        SW::$app->response->format = Response::FORMAT_JSON;
        return [
            'success' => true,
            'quantity' => $goodsList->quantity($item->getUniqueId()),
        ];
    }

    public function actionMinus($id, $listName)
    {
        $item = SW::$app->api->catalog->category->item($id);
        $goodsList = SW::$app->api->catalog->getGoodsList($listName);

        $goodsList->minus($item->getUniqueId());

        SW::$app->response->format = Response::FORMAT_JSON;
        return [
            'success' => true,
            'quantity' => $goodsList->quantity($item->getUniqueId()),
        ];
    }
}