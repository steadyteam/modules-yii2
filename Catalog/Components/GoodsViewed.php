<?php

namespace Steady\Modules\Catalog\Components;

use Steady\Engine\SW;
use Steady\Modules\Catalog\Base\GoodsInterface;
use Steady\Modules\Catalog\Base\GoodsList;

class GoodsViewed extends GoodsList
{
    /**
     * @inheritdoc
     */
    public function put(GoodsInterface $item, int $quantity = 1, $priority = true, $save = true)
    {
        return parent::put($item, $quantity, $priority, $save);
    }

    /**
     * @inheritdoc
     */
    public function fetch(?int $limit = null)
    {
        if ($limit === null) {
            $limit = SW::getModuleParams('catalog')['goodsViewed']['maxCount'];
        }
        return parent::fetch($limit);
    }
}