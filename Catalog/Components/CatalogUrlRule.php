<?php

namespace Steady\Modules\Catalog\Components;

use Steady\Engine\Base\Model;
use Steady\Engine\Validators\SlugAdvancedValidator;
use Steady\Modules\Catalog\Models\BrandModel;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\base\BaseObject;
use yii\web\UrlRuleInterface;

class CatalogUrlRule extends BaseObject implements UrlRuleInterface
{
    /**
     * @name \yii\web\UrlManager $manager
     * @name string $route
     * @name array $params
     * @return array|bool|string
     */
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'catalog/goods' && isset($params['slug'])) {
            return '/catalog/goods/' . $params['slug'];
        } else if ($route === 'catalog/category' && isset($params['slug'])) {
            return '/catalog/category' . $params['slug'];
        } else if ($route === 'catalog/brand' && isset($params['slug'])) {
            return '/catalog/brand/' . $params['slug'];
        }
        return false;
    }

    /**
     * @name \yii\web\UrlManager $manager
     * @name \yii\web\Request $request
     * @return array|bool
     * @throws \yii\base\InvalidConfigException
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        $url = explode('/', $pathInfo);

        foreach ($url as $item) {
            if (!preg_match(SlugAdvancedValidator::SLUG_ADVANCED_PATTERN, $item)) {
                return false;
            }
        }
        if ($url[0] == 'catalog' && isset($url[1]) && isset($url[2])) {
            if ($url[1] == 'goods') {
                return $this->route(['catalog/goods', ['slug' => $url[2]]], GoodsModel::class);
            } else if ($url[1] == 'brand') {
                return $this->route(['catalog/brand', ['slug' => $url[2]]], BrandModel::class);
            } else if ($url[1] == 'category') {
                return $this->route(['catalog/category', ['slug' => $url[2]]], CategoryModel::class);
            }
        } else if ($url[0] == 'catalog' && isset($url[1])) {
            return $this->route(['catalog/category', ['slug' => $url[1]]], CategoryModel::class);
        } else if ($url[0] == 'catalog/catalog' && count($url) === 1) {
            return ['catalog/catalog', ['slug' => 'catalog']];
        }
        return false;
    }

    /**
     * @name array $route
     * @name string|Model $class
     * @return array|bool
     */
    private function route($route, $class)
    {
        $id_slug = $route[1]['slug'];
        if (is_numeric($id_slug)) {
            $field = $class::primaryKey()[0];
        } else {
            $field = 'slug';
        }

        $model = $class::findOne([$field => $id_slug]);
        if ($model) {
            return $route;
        } else {
            return false;
        }
    }
}