<?php

namespace Steady\Modules\Catalog\Components;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\Modules\Page\Api\PageObject;
use Steady\Engine\Modules\Page\Components\PageController;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Api\BrandObject;
use Steady\Modules\Catalog\Api\CategoryObject;
use Steady\Modules\Catalog\Api\GoodsObject;
use Steady\Modules\Catalog\Database\GoodsSearch;
use Steady\Modules\Catalog\Models\OfferModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CatalogController extends PageController
{
    /**
     * @var array
     */
    protected $params = [];

    /**
     * @var string
     */
    protected $indexView = 'catalog/catalog';

    /**
     * @var string
     */
    protected $brandView = 'catalog/brand';

    /**
     * @var string
     */
    protected $categoryView = 'catalog/category';

    /**
     * @var string
     */
    protected $goodsView = 'catalog/goods';

    /**
     * @var string
     */
    protected $goodsPaginationView = '@backend/Resources/views/blocks/catalog/_goods_pagination.twig';

    /**
     * @var int
     */
    protected $goodsPageSize = 24;

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        return $this->actionCatalog('base');
    }

    /**
     * @name string $slug
     * @name bool $goods
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCatalog($slug = 'base', $goods = false)
    {
        $category = $this->getCategory($slug);

        $children = $category->children();

        $this->params = [
            'title' => $category->title,
            'object' => $category,
            'category' => $category,
            'children' => $children,
            'breadcrumbs' => $this->getBreadcrumbs($category),
        ];

        if ($goods) {
            $filter = [
                'categoryId' => $category->category_id,
                'brandId' => null,
                'tag' => null,
            ];
            $itemsOptions = $this->getGoodsOptions($filter);

            $params = [
                'goodsArray' => $category->goods($itemsOptions),
                'options' => $itemsOptions['params'],
            ];
            $this->params = ArrayHelper::merge($this->params, $params);
        }

        $this->preRender();

        return $this->render($this->indexView, $this->params);
    }

    /**
     * @name string $slug
     * @name string|null $category
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionBrand($slug, $category = null)
    {
        $brand = $this->getBrand($slug);
        if ($category) {
            $category = $this->getCategory($category);
        }

        $filter = [
            'categoryId' => $category->category_id ?? null,
            'brandId' => $brand->brand_id,
            'tag' => null,
        ];

        $itemsOptions = $this->getGoodsOptions($filter);

        $brothers = $brand->brothers(true);
        $children = $brand->children();

        $this->params = [
            'title' => $brand->title,
            'object' => $brand,
            'brand' => $brand,
            'brothers' => $brothers,
            'children' => $children,
            'brands' => $children ? $children : $brothers,
            'category' => $category,
            'categories' => $brand->categories(),
            'goodsArray' => $brand->goods($itemsOptions),
            'breadcrumbs' => $this->getBreadcrumbs($brand),
            'options' => $itemsOptions['params'],
        ];

        $this->preRender();

        return $this->render($this->brandView, $this->params);
    }

    /**
     * @name string $slug
     * @name string|null $brand
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionCategory($slug, $brand = null)
    {
        $category = $this->getCategory($slug);
        if ($brand) {
            $brand = $this->getBrand($brand);
        }

        $filter = [
            'categoryId' => $category->category_id,
            'brandId' => $brand->brand_id ?? null,
            'tag' => null,
        ];

        $itemsOptions = $this->getGoodsOptions($filter);

        $this->params = [
            'object' => $category,
            'goodsArray' => $category->goods($itemsOptions),
            'goodsCount' => $category->getPagination()->totalCount,
            'options' => $itemsOptions['params'],
        ];

        if (SW::$app->request->isAjax) {
            SW::$app->response->format = Response::FORMAT_JSON;
            return [
                'success' => true,
                'goodsPagination' => $this->renderFile($this->goodsPaginationView, $this->params),
                'goodsCount' => $this->params['goodsCount'],
                'attrs' => $category->model->getForFilterAttrs([
                    'minPrice' => $itemsOptions['params']['minPrice'],
                    'maxPrice' => $itemsOptions['params']['maxPrice'],
                    'attributes' => $itemsOptions['params']['attrs'],
                    'available' => $itemsOptions['params']['available'],
                ]),
            ];
        } else {
            $brothers = $category->brothers(true);
            $children = $category->children();

            $params = [
                'title' => $category->title,
                'category' => $category,
                'brothers' => $brothers,
                'children' => $children,
                'categories' => $children ? $children : $brothers,
                'brand' => $brand,
                'brands' => $category->brands(),
                'breadcrumbs' => $this->getBreadcrumbs($category),
            ];
            $this->params = ArrayHelper::merge($this->params, $params);

            $this->preRender();

            return $this->render($this->categoryView, $this->params);
        }
    }

    /**
     * @name $slug
     */
    public function actionTag($slug)
    {
        // TODO: Сделать базовый action для тэгов
    }

    /**
     * @name string $slug
     * @name null $offer
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionGoods($slug, $offer = null)
    {
        $goods = $this->getGoods($slug);
        $category = $goods->category_id ? $this->getCategory($goods->category_id) : null;
        $brand = $goods->brand_id ? $this->getBrand($goods->brand_id) : null;

        if ($offer) {
            /** @var OfferModel $item */
            foreach ($goods->offers() as $item) {
                if ($item->offer_id == $offer || $item->slug == $offer) {
                    $offer = $item;
                    break;
                }
            }
        }

        $goods->addToViewed();

        $this->params = [
            'object' => $goods,
            'offer' => $offer,
            'goods' => $goods,
            'category' => $category,
            'brand' => $brand,
            'goodsArray' => SW::$app->api->catalog->category->goodsPopular(4, $goods->category_id ?? null),
            'breadcrumbs' => $category ? $this->getBreadcrumbs($category, true) : [],
        ];

        $this->preRender();

        return $this->render($this->goodsView, $this->params);
    }

    /**
     * @name array $vars
     * @return string
     */
    protected function preRender($vars = []): string
    {
        if (isset($this->params['object']) && $this->params['object'] instanceof ApiObject) {
            $object = $this->params['object'];

            $this->makeSeo($object);

            if ($object instanceof BrandObject) {
                $this->makeBreadcrumbsForBrand();
            } else if ($object instanceof CategoryObject) {
                $this->makeBreadcrumbsForCategory();
            } else if ($object instanceof GoodsObject) {
                $this->makeBreadcrumbsForGoods();
            }
        }

        return true;
    }

    /**
     * @param ApiObject $object
     * @param PageObject|null $page
     * @param string|null $title
     * @param string|null $description
     * @param string|null $keywords
     * @param array $params
     * @return void
     */
    protected function makeSeo($object, $page = null, $title = null, $description = null, $keywords = null, $params = [])
    {
        $page = $this->pageObject;

        parent::makeSeo($object, $page, $title, $description, $keywords);
    }

    /**
     * @return string
     */
    protected function getPageSlug()
    {
        return SW::$app->controller->route;
    }

    /**
     * @name $id_slug
     * @return BrandObject
     * @throws NotFoundHttpException
     */
    protected function getBrand($id_slug)
    {
        $brand = SW::$app->api->catalog->brand->get($id_slug);
        if (!$brand) {
            throw new NotFoundHttpException('Brand not found.');
        }
        return $brand;
    }

    /**
     * @name $id_slug
     * @return CategoryObject
     * @throws NotFoundHttpException
     */
    protected function getCategory($id_slug)
    {
        $category = SW::$app->api->catalog->category->get($id_slug);
        if (!$category) {
            throw new NotFoundHttpException('Category not found.');
        }
        return $category;
    }

    /**
     * @name $id_slug
     * @return GoodsObject
     * @throws NotFoundHttpException
     */
    protected function getGoods($id_slug)
    {
        $goods = SW::$app->api->catalog->category->item($id_slug);
        if (!$goods) {
            throw new NotFoundHttpException('Goods not found.');
        }
        return $goods;
    }

    /**
     * @name array $filter
     * @return array
     */
    protected function getGoodsOptions(array $filter)
    {
        $request = SW::$app->request;
        $search = new GoodsSearch(true);

        $categoryIds = $request->get('categories') ? explode('__', $request->get('categories')) : $filter['categoryId'];
        $brandIds = $request->get('brands') ? explode('__', $request->get('brands')) : $filter['brandId'];
        $tags = $request->get('tags') ? explode('__', $request->get('tags')) : $filter['tag'];
        $params = $request->getQueryParams();//explode('__', $request->get('attrs'));
        $minPrice = $request->get('min-price');
        $maxPrice = $request->get('max-price');
        $available = $request->get('available');
        $sorts = $request->get('sort') ? explode('__', $request->get('sort')) : [];
        $viewType = $request->get('viewtype') ?? 'grid';

        // Формирум массив атрибутов
        $attrs = [];
        if (!empty($params)) {
            foreach ($params as $idx => $param) {
                if (!preg_match('/^a[0-9]/', $idx)) {
                    continue;
                }
                $attrId = substr($idx, 1);
                $attrValues = explode('__', $param);
                $attrs[$attrId] = $attrValues;
            }
        }
        $attrLikeWhere = $request->get('alw', false);

        // Формирум массив сортировки
        if (!empty($sorts)) {
            $result = [];
            foreach ($sorts as $idx => $sort) {
                $sort = explode('--', $sort);
                if (count($sort) >= 2) {
                    $result[$sort[0]] = $sort[1];
                }
            }
            $sorts = $result;
        }

        $search->category($categoryIds)
            ->brand($brandIds)
            ->tags($tags)
            ->price($minPrice, $maxPrice)
            ->attributes($attrs, $attrLikeWhere)
            ->available($available)
            ->sort($sorts);

        $query = $search->getQuery();

        $options = [
            'query' => $query,
            'pagination' => ['pageSize' => $this->goodsPageSize],
            'params' => [
                'categoryIds' => $categoryIds,
                'brandIds' => $brandIds,
                'tags' => $tags,
                'attrs' => $attrs,
                'available' => $available,
                'minPrice' => $minPrice,
                'maxPrice' => $maxPrice,
                'sorts' => $sorts,
                'viewType' => $viewType,
            ],
        ];

        return $options;
    }

    protected function makeBreadcrumbsForBrand()
    {
        $this->params['breadcrumbs'] = array_merge(
            [['label' => 'Бренды', 'url' => Url::to(['/brands'])]],
            $this->params['breadcrumbs']
        );
    }

    protected function makeBreadcrumbsForCategory()
    {
        $this->params['breadcrumbs'] = array_merge(
            [['label' => 'Каталог', 'url' => Url::to(['/catalog'])]],
            $this->params['breadcrumbs']
        );
    }

    protected function makeBreadcrumbsForGoods()
    {
        $brandReferrer = strstr(SW::$app->request->referrer, '/brand');
        if ($brandReferrer && $this->params['brand'] instanceof ApiObject) {
            $this->params['breadcrumbs'] = array_merge(
                [['label' => 'Бренды', 'url' => Url::to(['/brands'])]],
                $this->getBreadcrumbs($this->params['brand'], true)
            );
        } else if ($this->params['category'] instanceof ApiObject) {
            $this->params['breadcrumbs'] = array_merge(
                [['label' => 'Каталог', 'url' => Url::to(['/catalog'])]],
                $this->getBreadcrumbs($this->params['category'], true)
            );
        }
    }
}