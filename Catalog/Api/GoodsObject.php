<?php

namespace Steady\Modules\Catalog\Api;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Base\GoodsInterface;
use Steady\Modules\Catalog\Models\AttributesAssignModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use Steady\Modules\Catalog\Models\OfferModel;
use yii\base\InvalidConfigException;
use yii\helpers\Url;

/**
 * @property GoodsModel model
 */
class GoodsObject extends ApiObject implements GoodsInterface
{
    public $modelName = GoodsModel::class;

    public $goods_id;
    public $category_id;
    public $brand_id;
    public $slug;
    public $title;
    public $description;
    public $image;
    public $preview;
    public $content;
    public $price;
    public $discount;
    public $available;

    /**
     * @return CategoryObject|null
     */
    public function getCategory(): ?ApiObject
    {
        return SW::$app->api->catalog->category->get($this->category_id);
    }

    /**
     * @return BrandObject|null
     */
    public function getBrand(): ?ApiObject
    {
        return SW::$app->api->catalog->brand->get($this->brand_id);
    }

    /**
     * @name bool $scheme
     * @return string
     */
    public function url($scheme = false)
    {
        return Url::to(['/catalog/goods', 'slug' => $this->slug], $scheme);
    }

    /**
     * @return array
     */
    public function tags()
    {
        return $this->model->getTagsArray();
    }

    /**
     * @name string $alias
     * @return null|AttributesAssignModel
     */
    public function attr(string $alias)
    {
        return $this->model->getAttr($alias);
    }

    /**
     * @return array
     */
    public function attrs()
    {
        return $this->model->attrs();
    }

    /**
     * @return OfferModel[]
     */
    public function offers()
    {
        return $this->model->offers();
    }

    /**
     * @return string
     */
    public function getUniqueId()
    {
        return $this->model->getAlias() . '_' . $this->getId();
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->model->getPrice();
    }

    /**
     * @name $id
     * @return null|GoodsInterface
     */
    public static function loadItem($id): ?GoodsInterface
    {
        /** @var GoodsInterface $goods */
        $goods = SW::$app->api->catalog->category->item($id);

        return $goods;
    }

    /**
     * @return bool
     */
    public function addToViewed()
    {
        $viewed = SW::$app->api->catalog->viewed;
        $viewed->put($this);

        return true;
    }

}