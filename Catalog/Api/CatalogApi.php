<?php

namespace Steady\Modules\Catalog\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Base\GoodsList;
use Steady\Modules\Catalog\Components\GoodsViewed;
use Steady\Modules\Catalog\Storages\CookieStorage;

/**
 * @property CategoryService category
 * @property BrandService brand
 * @property GoodsList favourites
 * @property GoodsList compare
 * @property GoodsViewed viewed
 */
class CatalogApi extends Api
{
    const NAME_FAVOURITES = 'favourites';
    const NAME_COMPARE = 'compare';
    const NAME_VIEWED = 'viewed';

    /**
     * @var CategoryService
     */
    private $_categoryService;

    /**
     * @var BrandService
     */
    private $_brandService;

    /**
     * @var array
     */
    private $_goodsList = [];

    /**
     * @return CategoryService
     */
    public function getCategory()
    {
        if (!$this->_categoryService) {
            $this->_categoryService = new CategoryService();
        }
        return $this->_categoryService;
    }

    /**
     * @return BrandService
     */
    public function getBrand()
    {
        if (!$this->_brandService) {
            $this->_brandService = new BrandService();
        }
        return $this->_brandService;
    }

    /**
     * @return GoodsList
     */
    public function getFavourites()
    {
        return $this->getGoodsList(CatalogApi::NAME_FAVOURITES);
    }

    /**
     * @return GoodsList
     */
    public function getCompare()
    {
        return $this->getGoodsList(CatalogApi::NAME_COMPARE);
    }

    /**
     * @return GoodsViewed
     */
    public function getViewed()
    {
        $key = CatalogApi::NAME_VIEWED;
        if (!isset($this->_goodsList[$key]) || !$this->_goodsList[$key] instanceof GoodsViewed) {
            $viewedParams = SW::getModuleParams('catalog')['goodsViewed'];
            if (SW::$app->user->isGuest) {
                $storage = new CookieStorage($viewedParams['cookieKey'], $viewedParams['cookieExpire']);
            } else {
                $storage = new CookieStorage($viewedParams['cookieKey'], $viewedParams['cookieExpire']);
            }
            $this->_goodsList[$key] = new GoodsViewed($storage, SORT_DESC);
        }
        return $this->_goodsList[$key];
    }

    /**
     * @name string $key
     * @return GoodsList
     */
    public function getGoodsList(string $key)
    {
        if (!isset($this->_goodsList[$key]) || !$this->_goodsList[$key] instanceof GoodsList) {
            if (SW::$app->user->isGuest) {
                $storage = new CookieStorage($key);
            } else {
                $storage = new CookieStorage($key);
            }
            $this->_goodsList[$key] = new GoodsList($storage);
        }
        return $this->_goodsList[$key];
    }
}