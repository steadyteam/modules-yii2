<?php

namespace Steady\Modules\Catalog\Api;

use Steady\Modules\Catalog\Models\CategoryModel;

/**
 * @method CategoryObject get($slug)
 */
class CategoryService extends CommonService
{
    public $modelName = CategoryModel::class;

    public $objectName = CategoryObject::class;
}