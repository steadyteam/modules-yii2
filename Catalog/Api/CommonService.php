<?php

namespace Steady\Modules\Catalog\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\Base\ApiObject;
use Steady\Engine\Components\ItemsApiTrait;
use Steady\Engine\Components\TreeApiTrait;
use Steady\Engine\Modules\Field\Models\FieldModel;
use Steady\Modules\Catalog\CatalogModule;
use Steady\Modules\Catalog\Database\GoodsSearch;
use Steady\Modules\Catalog\Models\GoodsModel;

/**
 * @method GoodsObject item($id_alias)
 */
abstract class CommonService extends Api
{
    use TreeApiTrait;
    use ItemsApiTrait;

    public $moduleName = CatalogModule::class;

    public $itemModelName = GoodsModel::class;

    public $itemObjectName = GoodsObject::class;

    /**
     * @return ApiObject[]
     */
    public function goodsAll()
    {
        $query = GoodsModel::find();

        return $this->items([
            'query' => $query,
        ]);
    }

    /**
     * Получаем популярные товары в кол-ве $limit на основе поля $field
     * Если товаров с установленным полем не хватает до $limit, до добавляем остаток из первых товаров
     * @name int $limit
     * @name array $categoryIds
     * @name array $brandsIds
     * @name string $field
     * @return ApiObject[]
     */
    public function goodsPopular($limit = 8, $categoryIds = [], $brandsIds = [], $field = 'popular')
    {
        /** @var FieldModel $field */
        $field = FieldModel::find()->where(['alias' => (string)$field])->one();
        $attrs = $field ? [$field->field_id => array(1)] : [];

        $search = new GoodsSearch(true);
        $search->category($categoryIds)
            ->brand($brandsIds)
            ->attributes([$attrs]);

        $goodsIds = $search->getQuery()
            ->select(GoodsModel::tableName() . '.goods_id')
            ->limit($limit)
            ->asArray()
            ->all();

        if (count($goodsIds) < $limit) {
            $search = new GoodsSearch(true);
            $search->category($categoryIds)
                ->brand($brandsIds);

            $ids = $search->getQuery()
                ->select(GoodsModel::tableName() . '.goods_id')
                ->andWhere(['not in', 'goods_id', $goodsIds])
                ->limit($limit - count($goodsIds))
                ->asArray()
                ->all();

            $goodsIds = array_merge($goodsIds, $ids);
        }

        $query = GoodsModel::find()
            ->where(['goods_id' => $goodsIds])
            ->limit($limit);

        return $this->items([
            'query' => $query,
        ]);
    }
}