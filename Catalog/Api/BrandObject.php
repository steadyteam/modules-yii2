<?php

namespace Steady\Modules\Catalog\Api;

use Steady\Engine\Base\ApiObject;
use Steady\Modules\Catalog\Interfaces\Goodsable;
use Steady\Engine\Components\ItemsObjectTrait;
use Steady\Engine\Components\TreeObjectInterface;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\BrandModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\helpers\Url;

/**
 * @property BrandModel model
 */
class BrandObject extends ApiObject implements TreeObjectInterface, Goodsable
{
    use TreeObjectTrait;
    use ItemsObjectTrait {
        items as protected traitItems;
    }

    public $modelName = BrandModel::class;

    public $itemModelName = GoodsModel::class;

    public $itemObjectName = GoodsObject::class;

    public $brand_id;
    public $slug;
    public $title;
    public $image;
    public $content;

    /**
     * @name bool $scheme
     * @return string
     */
    public function url($scheme = false)
    {
        return Url::to(['/catalog/brand', 'slug' => $this->slug], $scheme);
    }

    /**
     * @name array $options
     * @return GoodsObject[]|ApiObject[]
     */
    public function goods($options = [])
    {
        if (!isset($options['query'])) {
            $options['query'] = $this->model->goods(true);
        }

        return $this->traitItems($options);
    }

    /**
     * @return int
     */
    public function goodsCount()
    {
        return $this->model->goodsCount();
    }

    /**
     * @return ApiObject[]|CategoryObject[]
     */
    public function categories()
    {
        return SW::$app->api->catalog->category->createObjectsFromModels($this->model->categories());
    }

    /**
     * @return array
     */
    public function attrs()
    {
        return $this->model->attrs();
    }

    /**
     * @return array
     */
    public function priceRange()
    {
        return $this->model->priceRange();
    }
}