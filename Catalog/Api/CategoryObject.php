<?php

namespace Steady\Modules\Catalog\Api;

use Steady\Engine\Base\ApiObject;
use Steady\Modules\Catalog\Interfaces\Goodsable;
use Steady\Engine\Components\ItemsObjectTrait;
use Steady\Engine\Components\TreeObjectInterface;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\helpers\Url;

/**
 * @property CategoryService api
 * @property CategoryModel model
 */
class CategoryObject extends ApiObject implements TreeObjectInterface, Goodsable
{
    use TreeObjectTrait;
    use ItemsObjectTrait {
        items as protected traitItems;
    }

    public $modelName = CategoryModel::class;

    public $itemModelName = GoodsModel::class;

    public $itemObjectName = GoodsObject::class;

    public $category_id;
    public $slug;
    public $title;
    public $image;
    public $content;

    /**
     * @name bool $scheme
     * @return string
     */
    public function url($scheme = false)
    {
        return Url::to(['/catalog/category', 'slug' => $this->slug], $scheme);
    }

    /**
     * @name array $options
     * @return GoodsObject[]|ApiObject[]
     */
    public function goods($options = [])
    {
        if (!isset($options['query'])) {
            $options['query'] = $this->model->goods(true);
        }

        return $this->traitItems($options);
    }

    /**
     * @return int
     */
    public function goodsCount()
    {
        return $this->model->goodsCount();
    }

    /**
     * @return ApiObject[]|CategoryObject[]
     */
    public function brands()
    {
        return SW::$app->api->catalog->brand->createObjectsFromModels($this->model->brands());
    }

    /**
     * @return array
     */
    public function attrs()
    {
        return $this->model->attrs();
    }

    /**
     * @return array
     */
    public function priceRange()
    {
        return $this->model->priceRange();
    }
}