<?php

namespace Steady\Modules\Catalog\Api;

use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\BrandModel;

/**
 * @method BrandObject get($slug)
 */
class BrandService extends CommonService
{
    public $modelName = BrandModel::class;

    public $objectName = BrandObject::class;

    public function findInCategory($id_alias, $depth = null)
    {
        $category = SW::$app->api->catalog->category->get($id_alias);
        $brands = $category->brands();

        return $this->createObjectsFromModels($brands);
    }
}