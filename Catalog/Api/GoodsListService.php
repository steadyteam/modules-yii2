<?php

namespace Steady\Modules\Catalog\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Base\GoodsList;
use Steady\Modules\Catalog\CatalogModule;
use Steady\Modules\Catalog\Storages\CookieStorage;

class GoodsListService extends Api
{

    public $moduleName = CatalogModule::class;

    /**
     * @var string
     */
    private $_name;

    /**
     * @var GoodsList
     */
    private $_goodsList;

    /**
     * GoodsListService constructor
     * @name $name
     * @name array $config
     */
    public function __construct($name, array $config = [])
    {
        $this->_name = $name;
        parent::__construct($config);
    }

    /**
     * @name $user
     * @return GoodsList
     */
    public function get($user = null): GoodsList
    {
        if (!$this->_goodsList) {
            if (SW::$app->user->isGuest) {
                $storage = new CookieStorage($this->_name);
            } else {
                $storage = new CookieStorage($this->_name);
            }
            $this->_goodsList = new GoodsList($storage);
        }
        return $this->_goodsList;
    }
}