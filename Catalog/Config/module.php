<?php

return [
    'params' => [
        'goodsViewed' => [
            'cookieKey' => 'goods_viewed',
            'cookieExpire' => 3600 * 24 * 3,
            'maxCount' => 30,
        ],
    ],
];