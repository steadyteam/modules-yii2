<?php

$this->title = $form['model']->title;

?>

<?= $this->render('_menu') ?>

<?php if (!empty($this->params['submenu'])) {
    echo $this->render('_submenu', $form, $this->context);
} ?>
<?= $this->render('_form', $form) ?>