<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;

?>

<div class="action-list">
    <?php if ($action === 'index') : ?>
        <a class="btn btn-primary" href="<?= Url::to(['/admin/' . $module]) ?>">
            <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?= SW::t('admin', 'Categories') ?>
        </a>
        <?php if (isset($model) && $model->primaryKey) : ?>
            <a class="btn btn-warning" href="<?= Url::to(["/admin/$module/a/edit", 'id' => $model->primaryKey]) ?>">
                <i class="glyphicon glyphicon-edit font-12"></i>
                <?= SW::t('admin', 'Edit category') ?>
            </a>
            <a class="btn btn-success"
               href="<?= Url::to(["/admin/$module/goods/create", 'id' => $model->primaryKey]) ?>">
                <i class="glyphicon glyphicon-plus font-12"></i>
                <?= SW::t('admin/catalog', 'Create product') ?>
            </a>
        <?php endif; ?>
    <?php else : ?>
        <a class="btn btn-primary"
           href="<?= Url::to(["/admin/$module/goods", 'id' => $model->category->category_id]) ?>">
            <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?= SW::t('admin/catalog', 'Category') ?>
        </a>
    <?php endif; ?>
    <?php if ($action === 'offers') : ?>
        <a class="btn btn-success"
           href="<?= Url::to(["/admin/$module/offer/create", 'id' => $model->primaryKey]) ?>">
            <i class="glyphicon glyphicon-plus font-12"></i>
            <?= SW::t('admin/catalog', 'Create offer') ?>
        </a>
    <?php endif; ?>
</div>
<br/>