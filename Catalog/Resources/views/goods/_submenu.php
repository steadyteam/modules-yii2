<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;
?>

<ul class="nav nav-tabs">
    <li <?= ($action === 'edit') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['/admin/' . $module . '/goods/edit', 'id' => $model->primaryKey]) ?>">
            <?= SW::t('admin', 'Edit') ?>
        </a>
    </li>
    <li <?= ($action === 'photos') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['/admin/' . $module . '/goods/photos', 'id' => $model->primaryKey]) ?>">
            <span class="glyphicon glyphicon-camera"></span> <?= SW::t('admin', 'Photos') ?>
        </a>
    </li>
    <li <?= ($action === 'offers') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['/admin/' . $module . '/goods/offers', 'id' => $model->primaryKey]) ?>">
            Модификации
        </a>
    </li>
</ul>
<br>