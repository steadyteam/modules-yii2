<?php

use Steady\Engine\SW;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = SW::t('admin/catalog', 'Products');

$module = $this->context->module->id;

?>

<?= $this->render('_menu') ?>

<?php if ($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <?php if (IS_ROOT) : ?>
                <th width="50">#</th>
            <?php endif; ?>
            <th><?= SW::t('admin', 'Title') ?></th>
            <?php if (IS_ROOT) : ?>
                <th><?= SW::t('admin', 'Slug') ?></th>
                <th width="30"></th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $item) : ?>
            <tr>
                <?php if (IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td>
                    <a href="<?= Url::to(['/admin/' . $module . '/goods/edit', 'id' => $item->primaryKey]) ?>">
                        <?= $item->title ?>
                    </a>
                </td>
                <?php if (IS_ROOT) : ?>
                    <td><?= $item->slug ?? $this->alias ?></td>
                    <td>
                        <a href="<?= Url::to(['/admin/' . $module . '/goods/delete', 'id' => $item->primaryKey]) ?>"
                           class="glyphicon glyphicon-remove confirm-delete"
                           title="<?= SW::t('admin', 'Delete item') ?>"></a>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?= LinkPager::widget(['pagination' => $data->pagination]) ?>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>