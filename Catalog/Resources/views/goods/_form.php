<?php

use Steady\Admin\Helpers\HtmlHelper;
use Steady\Admin\Widgets\DateTimeWidget;
use Steady\Admin\Widgets\RedactorWidget;
use Steady\Admin\Widgets\SeoFormWidget;
use Steady\Admin\Widgets\TagsInput;
use Steady\Engine\Helpers\Image;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$settings = $this->context->module->settings;
$module = $this->context->module->id;
$categoriesValid = ArrayHelper::map($categoriesValid, 'category_id', 'title');
//$brandsValid = ArrayHelper::map($brandsValid, 'brand_id', 'title');
$brandsValid = ArrayHelper::map(SW::$app->api->catalog->brand->children('base'), 'brand_id', 'title');

?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form'],
]); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'slug') ?>
<?= $form->field($model, 'category_id')->dropDownList($categoriesValid, ['prompt' => '- SelectWidget category -']) ?>
<?= $form->field($model, 'brand_id')->dropDownList($brandsValid, ['prompt' => '- SelectWidget brand -']) ?>

<?= $form->field($model, 'tagsName')->widget(TagsInput::class) ?>

<?= HtmlHelper::generateFieldForm($model) ?>

<?php if ($settings['itemThumb']) : ?>
    <?php if ($model->image) : ?>
        <? $clearUrl = Url::to(['/admin/' . $module . '/goods/clear-image', 'id' => $model->primaryKey]) ?>
        <? $clearText = SW::t('admin', 'Clear image') ?>
        <div class="form-group">
            <img src="<?= Image::thumb($model->image, 240) ?>">
        </div>
        <div class="form-group">
            <a href="<?= $clearUrl ?>" class="text-danger confirm-delete" title="<?= $clearText ?>">
                <?= $clearText ?>
            </a>
        </div>
    <?php endif; ?>
    <?= $form->field($model, 'image')->fileInput() ?>
<?php endif; ?>

        <!-- TODO: Возможно поле стоит удалить
<?php /*if ($settings['itemDescription']) : */ ?><!--
    <? /*= $form->field($model, 'description')->widget(RedactorWidget::class, [
        'options' => [
            'minHeight' => 300,
            'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'catalog'], true),
            'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'catalog'], true),
            'plugins' => ['fullscreen']
        ]
    ]) */ ?>
--><?php /*endif; */ ?>

<?= $form->field($model, 'preview')->widget(RedactorWidget::class, [
    'options' => [
        'minHeight' => 100,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
    ],
]) ?>
<?= $form->field($model, 'content')->widget(RedactorWidget::class, [
    'options' => [
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
    ],
]) ?>

<? /*= $form->field($model, 'available') */ ?>
<?= $form->field($model, 'price') ?>
<? /*= $form->field($model, 'discount') */ ?>

<?= $form->field($model, 'time')->widget(DateTimeWidget::class); ?>

<?= SeoFormWidget::widget(['model' => $model]) ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>