<?php

use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\helpers\Url;

/**
 * @var $goods GoodsModel
 */

$this->title = SW::t('admin/catalog', 'Offers') . ' ' . $goods->title;;

$module = $this->context->module->id;
$offers = $goods->offers();

?>

<?= $this->render('_menu', ['model' => $goods]) ?>
<?= $this->render('_submenu', ['model' => $goods]) ?>

<?php if ($offers) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= SW::t('admin', 'Title') ?></th>
            <th><?= SW::t('admin', 'Slug') ?></th>
            <th><?= SW::t('admin', 'Price') ?></th>
            <th width="30"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($offers as $offer) : ?>
            <tr>
                <td><?= $offer->primaryKey ?></td>
                <td>
                    <a href="<?= Url::to(['/admin/' . $module . '/offer/', 'id' => $offer->primaryKey]) ?>">
                        <?= $offer->title ?>
                    </a>
                </td>
                <td><?= $offer->slug ?></td>
                <td><?= number_format($offer->price, 0, ',', ' ') ?></td>
                <td>
                    <a href="<?= Url::to(['/admin/' . $module . '/offer/delete', 'id' => $offer->primaryKey]) ?>"
                       class="glyphicon glyphicon-remove confirm-delete"
                       title="<?= SW::t('admin', 'Delete offer') ?>"></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>