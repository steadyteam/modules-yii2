<?php

use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;

$module = $this->context->module->id;

?>

<?= $this->render('_menu', ['model' => $model]) ?>

<?php if (count($model->goods())) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <?php if (IS_ROOT) : ?>
                <th width="50">#</th>
            <?php endif; ?>
            <th><?= SW::t('admin', 'Name') ?></th>
            <th width="100"><?= SW::t('admin', 'Status') ?></th>
            <th width="120"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->goods() as $item) : ?>
            <tr data-id="<?= $item->primaryKey ?>">
                <?php if (IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td>
                    <a href="<?= Url::to(['/admin/' . $module . '/goods/edit', 'id' => $item->primaryKey]) ?>"><?= $item->title ?></a>
                </td>
                <td class="status">
                    <?= Html::checkbox('', $item->status == GoodsModel::STATUS_ON, [
                        'class' => 'switch',
                        'data-id' => $item->primaryKey,
                        'data-link' => Url::to(['/admin/' . $module . '/goods']),
                    ]) ?>
                </td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="<?= Url::to(['/admin/' . $module . '/goods/up', 'id' => $item->primaryKey, 'category_id' => $model->primaryKey]) ?>"
                           class="btn btn-default move-up" title="<?= SW::t('admin', 'Move up') ?>"><span
                                    class="glyphicon glyphicon-arrow-up"></span></a>
                        <a href="<?= Url::to(['/admin/' . $module . '/goods/down', 'id' => $item->primaryKey, 'category_id' => $model->primaryKey]) ?>"
                           class="btn btn-default move-down" title="<?= SW::t('admin', 'Move down') ?>"><span
                                    class="glyphicon glyphicon-arrow-down"></span></a>
                        <a href="<?= Url::to(['/admin/' . $module . '/goods/delete', 'id' => $item->primaryKey]) ?>"
                           class="btn btn-default confirm-delete" title="<?= SW::t('admin', 'Delete item') ?>"><span
                                    class="glyphicon glyphicon-remove"></span></a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>