<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;
?>

<ul class="nav nav-tabs">
    <li <?= ($action === 'edit' || $action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['/admin/' . $module . '/offer/edit', 'id' => $model->primaryKey]) ?>">
            <?= SW::t('admin', 'Edit') ?>
        </a>
    </li>
    <li <?= ($action === 'photos') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['/admin/' . $module . '/offer/photos', 'id' => $model->primaryKey]) ?>">
            <span class="glyphicon glyphicon-camera"></span> <?= SW::t('admin', 'Photos') ?>
        </a>
    </li>
</ul>
<br>