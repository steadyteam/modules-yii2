<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;

?>

<div class="action-list">
    <a class="btn btn-primary" href="<?= Url::to(["/admin/$module/goods/offers", 'id' => $model->goods_id]) ?>">
        <i class="glyphicon glyphicon-chevron-left font-12"></i>
        <?= SW::t('admin', 'Product') ?>
    </a>
</div>
<br/>