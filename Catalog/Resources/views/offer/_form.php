<?php

use Steady\Admin\Helpers\HtmlHelper;
use Steady\Admin\Widgets\RedactorWidget;
use Steady\Admin\Widgets\SeoFormWidget;
use Steady\Admin\Widgets\TagsInput;
use Steady\Engine\Helpers\Image;
use Steady\Engine\SW;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$settings = $this->context->module->settings;
$module = $this->context->module->id;

?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form'],
]); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'slug') ?>

<?= $form->field($model, 'tagsName')->widget(TagsInput::class) ?>

<?= HtmlHelper::generateFieldForm($model) ?>

<?php if ($settings['itemThumb']) : ?>
    <?php if ($model->image) : ?>
        <? $clearUrl = Url::to(['/admin/' . $module . '/offer/clear-image', 'id' => $model->primaryKey]) ?>
        <? $clearText = SW::t('admin', 'Clear image') ?>
        <div class="form-group">
            <img src="<?= Image::thumb($model->image, 240) ?>">
        </div>
        <div class="form-group">
            <a href="<?= $clearUrl ?>" class="text-danger confirm-delete" title="<?= $clearText ?>">
                <?= $clearText ?>
            </a>
        </div>
    <?php endif; ?>
    <?= $form->field($model, 'image')->fileInput() ?>
<?php endif; ?>

<?= $form->field($model, 'description')->widget(RedactorWidget::class, [
    'options' => [
        'minHeight' => 100,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
    ],
]) ?>
<?= $form->field($model, 'content')->widget(RedactorWidget::class, [
    'options' => [
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
    ],
]) ?>

<? /*= $form->field($model, 'available') */ ?>
<?= $form->field($model, 'price') ?>
<? /*= $form->field($model, 'discount') */ ?>

<?= SeoFormWidget::widget(['model' => $model]) ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>