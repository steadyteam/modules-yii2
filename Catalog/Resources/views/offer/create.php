<?php

use Steady\Engine\SW;

$this->title = SW::t('admin/catalog', 'Create item');
?>
<?= $this->render('_menu', $form) ?>
<?= $this->render('_form', $form) ?>