<?php

use Steady\Admin\Widgets\PhotosWidget;
use Steady\Engine\SW;

$this->title = SW::t('admin', 'Photos') . ' ' . $model->title;
?>

<?= $this->render('_menu') ?>
<?= $this->render('_submenu', ['model' => $model]) ?>

<?= PhotosWidget::widget(['model' => $model]) ?>