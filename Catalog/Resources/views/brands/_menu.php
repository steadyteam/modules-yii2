<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;
?>

<div class="action-list">
    <?php if ($action === 'index') : ?>
        <a class="btn btn-primary" href="<?= Url::to(['/admin/' . $module]) ?>">
            <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?= SW::t('admin', 'Categories') ?>
        </a>
        <a class="btn btn-success"
           href="<?= Url::to(["/admin/$module/brands/create"]) ?>">
            <i class="glyphicon glyphicon-plus font-12"></i>
            <?= SW::t('admin/catalog', 'Create brand') ?>
        </a>
    <?php elseif ($action === 'create' || $action === 'edit') : ?>
        <a class="btn btn-primary" href="<?= Url::to(["/admin/$module/brands"]) ?>">
            <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?= SW::t('admin/catalog', 'Brands') ?>
        </a>
    <?php endif; ?>
</div>
<br/>