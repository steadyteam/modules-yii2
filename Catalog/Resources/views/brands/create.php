<?php

use Steady\Engine\SW;

$this->title = SW::t('admin', 'Create brand');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>