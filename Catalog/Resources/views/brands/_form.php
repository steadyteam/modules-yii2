<?php

use Steady\Admin\Helpers\HtmlHelper;
use Steady\Admin\Widgets\RedactorWidget;
use Steady\Admin\Widgets\SeoFormWidget;
use Steady\Engine\Helpers\Image;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$class = $this->context->modelName;
$settings = $this->context->module->settings;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'slug') ?>
<?= $form->field($model, 'parent_id')->dropDownList(
    ArrayHelper::map($validParents, 'brand_id', 'title'),
    ['prompt' => '- SelectWidget category -']) ?>

<?php if ($settings['categoryThumb']) : ?>
    <?php if ($model->image) : ?>
        <? $clearUrl = Url::to(['/admin/' . $this->context->moduleName . '/a/clear-image', 'id' => $model->primaryKey]) ?>
        <? $clearText = SW::t('admin', 'Clear image') ?>
        <div class="form-group">
            <img src="<?= Image::thumb($model->image, 240) ?>">
        </div>
        <div class="form-group">
            <a href="<?= $clearUrl ?>" class="text-danger confirm-delete" title="<?= $clearText ?>">
                <?= $clearText ?>
            </a>
        </div>
    <?php endif; ?>
    <?= $form->field($model, 'image')->fileInput() ?>
<?php endif; ?>

<?= $form->field($model, 'content')->widget(RedactorWidget::class, [
    'options' => [
        'minHeight' => 300,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'catalog'], true),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'catalog'], true),
        'plugins' => ['fullscreen'],
    ],
]) ?>

<?= HtmlHelper::generateFieldForm($model) ?>

<?= SeoFormWidget::widget(['model' => $model]) ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-success btn-c-md']) ?>
<?php ActiveForm::end(); ?>