<?php

use Steady\Engine\SW;

$this->title = SW::t('admin', 'Edit brand');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_submenu', $form) ?>

<?= $this->render('_form', $form) ?>