<?php
return [
    'Catalog' => 'Каталог',
    'Catalog updated' => 'Каталог обновлен',
    'Category fields' => 'Поля категории',
    'Manage fields' => 'Редактировать поля',

    'Category' => 'Категория',
    'Create category' => 'Создать категорию',
    'Parent category' => 'Родительская категория',

    'Brand' => 'Бренд',

    'Products' => 'Товары',
    'Products list' => 'Список товаров',
    'Create product' => 'Создать товар',

    'Create item' => 'Добавить элемент',
    'Item created' => 'Новая запись успешно создана',
    'Item updated' => 'Запись обновлена',
    'Item deleted' => 'Запись удалена',

    'Available' => 'Доступно',
    'Price' => 'Цена',
    'Discount' => 'Скидка',
];