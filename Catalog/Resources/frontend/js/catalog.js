function FilterForm(urlHandler) {
    this.urlHandler = urlHandler;
    this.currentParams = null;

    this.init = function () {
        this.currentParams = this.urlHandler.explode(this.urlHandler.parseParams());
        this.resetCheckbox();

        $.each(this.currentParams, function (key, value) {
            value.forEach(function (item) {
                var $el = $('.js_catalog_filter[data-type="' + key + '"][data-value="' + item + '"]');
                $el.find('.checkbox').prop('checked', true);
            });
        });
    };

    this.resetCheckbox = function () {
        $('.js_catalog_filter').find('.checkbox').prop('checked', false);
    };

    this.setItem = function ($el) {
        var type = $el.data('type');
        var value = $el.data('value');
        var checked = $el.find('.checkbox').prop('checked');

        var that = this;
        if (checked) {
            if (!that.currentParams[type]) {
                that.currentParams[type] = [];
            }
            if ($.inArray(value, that.currentParams[type]) === -1) {
                that.currentParams[type].push(value);
            }
        } else {
            if (that.currentParams[type]) {
                that.currentParams[type].splice($.inArray(value, that.currentParams[type]), 1);
                if (that.currentParams[type].length === 0) {
                    delete that.currentParams[type];
                }
            }
        }

        history.pushState('', '', window.location.pathname + '?' + $.name(this.urlHandler.implode(this.currentParams)));
        this.urlHandler.explode(this.currentParams);

        window.location.reload();
    };

    this.reset = function () {
        history.pushState('', '', window.location.pathname);
        window.location.reload();
    };
}

$(window).load(function () {

    var urlHandler = new UrlHandler();
    var filterForm = new FilterForm(urlHandler);

    filterForm.init();

    $('.js_catalog_filter').on('click', function (e) {
        var checkbox = $(this).find('.checkbox');
        checkbox.prop('checked', !checkbox.prop('checked'));
        e.preventDefault();

        filterForm.setItem($(this));
        return true;
    });

    $('.js_catalog_filter_reset').on('click', function (e) {
        e.preventDefault();

        filterForm.reset();
        return true;
    });
});