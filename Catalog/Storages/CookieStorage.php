<?php

namespace Steady\Modules\Catalog\Storages;

use Steady\Engine\SW;
use Steady\Modules\Catalog\Api\GoodsObject;
use Steady\Modules\Catalog\Base\GoodsInterface;
use Steady\Modules\Catalog\Base\GoodsListInterface;
use Steady\Modules\Catalog\Base\StorageInterface;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\web\Cookie;

class CookieStorage implements StorageInterface
{
    /**
     * @var string|null
     */
    private $_key = null;

    /**
     * @var integer|null
     */
    private $_expire = 0;

    /**
     * CookieStorage constructor.
     * @name $key
     * @name int $expire
     */
    public function __construct($key, $expire = 0)
    {
        $this->_key = $key;
        $this->_expire = $expire;
    }

    /**
     * @name GoodsListInterface $goodsList
     * @return bool
     */
    public function save(GoodsListInterface $goodsList)
    {
        $cookieData = [];

        /** @var GoodsInterface[] $goodsArray */
        $goodsArray = $goodsList->fetch();

        if (!empty($goodsArray)) {
            foreach ($goodsArray as $goods) {
                $data['class'] = get_class($goods);
                $data['id'] = $goods->getId();
                $data['quantity'] = $goodsList->quantity($goods->getUniqueId());
                $cookieData[] = $data;
            }
        }

        SW::$app->response->cookies->add(new Cookie([
            'name' => $this->_key,
            'value' => serialize($cookieData),
            'expire' => $this->_expire ? time() + $this->_expire : $this->_expire,
        ]));

        return true;
    }

    /**
     * @name GoodsListInterface $goodsList
     * @name bool $exists Флаг, если true добавляем товар, даже если такой уже имеется,
     * тем самым увеличиаем его кол-во, если false и товар уже есть, не добавляем его
     * @return bool
     */
    public function load(GoodsListInterface $goodsList, $exists = true)
    {
        $cookiesData = unserialize(SW::$app->request->cookies->get($this->_key));

        if (empty($cookiesData)) {
            return false;
        }

        $goodsIds = [];
        $goodsData = [];
        foreach ($cookiesData as $data) {
            if ($this->isValidData($data)) {
                $goodsIds[] = $data['id'];
                $goodsData[$data['id']] = $data;
            }
        }

        $goodsModels = GoodsModel::find()
            ->where(['goods_id' => $goodsIds])
            ->all();

        /** @var GoodsObject[] $goodsArray */
        $goodsArray = SW::$app->api->catalog->createObjectsFromModels($goodsModels, GoodsObject::class);

        foreach ($goodsArray as $goods) {
            if ($exists || (!$exists && !$goodsList->get($goods->getUniqueId()))) {
                $goodsList->put($goods, $goodsData[$goods->goods_id]['quantity'], false);
            }
        }

        return true;
    }

    /**
     * @name GoodsListInterface $goodsList
     * @return bool
     */
    public function synch(GoodsListInterface $goodsList)
    {
        $this->load($goodsList, false);
        $this->clear();

        return true;
    }

    /**
     * @return bool
     */
    public function clear()
    {
        SW::$app->response->cookies->remove($this->_key);

        return true;
    }

    /**
     * @name array $data
     * @return bool
     */
    private function isValidData(array $data)
    {
        if (isset($data['class']) && isset($data['id']) && isset($data['quantity'])
            && class_exists($data['class'])) {
            return true;
        }

        return false;
    }
}