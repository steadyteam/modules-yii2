<?php

namespace Steady\Modules\Catalog\Widgets;

use Steady\Engine\SW;
use Steady\Modules\Catalog\Api\CatalogApi;
use Steady\Modules\Catalog\Base\GoodsInterface;
use Steady\Modules\Shop\Components\Cart;
use yii\base\Widget;
use yii\helpers\Url;

class FavouritesButtonWidget extends Widget
{
    /**
     * @var Cart
     */
    public $favourites;

    /**
     * @var GoodsInterface
     */
    public $item;

    /**
     * @var array
     */
    public $classCss = [];

    /**
     * @var array
     */
    public $text = ['Добавить в избранное', 'Убрать из избранного'];

    /**
     * @var bool
     */
    public $put;

    /**
     * @throws \Exception
     */
    public function init()
    {
        parent::init();

        $this->favourites = SW::$app->api->catalog->favourites;
        $this->put = $this->favourites->get($this->item->getUniqueId());
    }

    /**
     * @return string|void
     */
    public function run()
    {
        $addUrl = Url::to(['/catalog-action/add-to-list', 'id' => $this->item->getId(),
            'listName' => CatalogApi::NAME_FAVOURITES]);
        $removeUrl = Url::to(['/catalog-action/remove-from-list', 'id' => $this->item->getId(),
            'listName' => CatalogApi::NAME_FAVOURITES]);

        if ($this->put) {
            $url = $removeUrl;
            $newUrl = $addUrl;
        } else {
            $url = $addUrl;
            $newUrl = $removeUrl;
        }

        $class = $this->getValue($this->classCss, $this->put);
        $text = $this->getValue($this->text, $this->put);
        $action = $this->put ? 'remove' : 'add';
        $newClass = $this->getValue($this->classCss, !$this->put);
        $newText = $this->getValue($this->text, !$this->put);
        $changeQuantity = $this->put ? -$this->favourites->quantity($this->item->getUniqueId()) : 1;
        $quantity = $this->put ? $this->favourites->quantity($this->item->getUniqueId()) : 1;

        echo "<a href='$url' class='$class' data-action='$action' data-new-text='$newText' data-new-class='$newClass' 
            data-new-url='$newUrl' data-change-quantity='$changeQuantity' data-quantity='$quantity'>$text</a>";
    }

    /**
     * @name $value
     * @name $condition
     * @return mixed
     */
    private function getValue($value, $condition)
    {
        return is_array($value) ? $value[$condition ? 1 : 0] : $value;
    }
}