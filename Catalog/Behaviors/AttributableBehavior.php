<?php

namespace Steady\Modules\Catalog\Behaviors;

use Steady\Modules\Catalog\Models\AttributeModel;
use Steady\Modules\Catalog\Models\AttributesAssignModel;
use Steady\Modules\Catalog\Models\BrandModel;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\base\Behavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property CategoryModel|BrandModel|GoodsModel owner
 * @property AttributeModel[] attributesRelation
 * @property array attrs
 */
class AttributableBehavior extends Behavior
{
    /**
     * @var array
     */
    private $_attrs = [];

    /**
     * @return array
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function afterSave()
    {
        if ($this->_attrs && count($this->_attrs)) {
            foreach ($this->_attrs as $id => $data) {
                if (!isset($data['alias']) || !isset($data['content'])) {
                    $owner = $this->owner->getAlias();
                    throw new \Exception("Undefined attribute! Owner = $owner, id = $id");
                }
                $assign = $this->setAttr($data['alias'], $data['content']);
            }
        }
    }

    /**
     *
     */
    public function beforeDelete()
    {

    }

    /**
     * @return ActiveQuery
     */
    public function getAttributesRelation(): ActiveQuery
    {
        return $this->owner->hasMany(AttributeModel::class, ['attribute_id' => 'attribute_id'])
            ->via('attributesAssignsRelation');
    }

    /**
     * @return ActiveQuery
     */
    public function getAttributesAssignsRelation(): ActiveQuery
    {
        return $this->owner->hasMany(AttributesAssignModel::class, ['owner_id' => $this->owner->primaryKey()[0]])
            ->where([AttributesAssignModel::tableName() . '.class_alias' => $this->owner->getAlias()]);
    }

    /**
     * @return array
     */
    public function getAttributesForOwner()
    {
        return AttributesAssignModel::getDataForOwner($this->owner->primaryKey, $this->owner->getAlias());
    }

    /**
     * @name string $alias
     * @name string $title
     * @name int $type
     * @return null|AttributeModel
     * @throws \yii\db\Exception
     */
    public function addAttr(string $alias, string $title, int $type): ?AttributeModel
    {
        $attribute = AttributeModel::findOne(['alias' => $alias, 'type' => $type]);
        if (!$attribute) {
            $attribute = AttributeModel::create($alias, $title, $type);
        }
        $assign = AttributesAssignModel::getAssign($attribute->attribute_id, $this->owner->primaryKey,
            $this->owner->getAlias());
        if (!$assign) {
            $assign = AttributesAssignModel::create($attribute->attribute_id, $this->owner->primaryKey,
                $this->owner->getAlias());
        }

        if ($attribute->saveEx() && $assign->saveEx()) {
            return $attribute;
        }

        return null;
    }

    /**
     * @name string $alias
     * @return null|AttributesAssignModel
     */
    public function getAttr(string $alias)
    {
        $attribute = AttributeModel::findOne(['alias' => $alias]);
        if (!$attribute) {
            return null;
        }
        $assign = AttributesAssignModel::getAssign($attribute->attribute_id, $this->owner->primaryKey,
            $this->owner->getAlias());
        if (!$assign) {
            return null;
        }

        return $assign;
    }

    /**
     * @name string $alias
     * @name string $content
     * @return AttributesAssignModel
     * @throws \yii\db\Exception
     */
    public function setAttr(string $alias, string $content): ?AttributesAssignModel
    {
        $attribute = AttributeModel::findOne(['alias' => $alias]);
        if (!$attribute) return null;

        $assign = AttributesAssignModel::getAssign($attribute->attribute_id, $this->owner->primaryKey, $this->owner->getAlias());
        if (!$assign) {
            $assign = AttributesAssignModel::create($attribute->attribute_id, $this->owner->primaryKey, $this->owner->getAlias());
        }

        $assign->content = $content;
        $assign->saveEx();

        return $assign;
    }

    /**
     * @name string $alias
     * @return bool|false|int
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function deleteAttr(string $alias)
    {
        $attribute = AttributeModel::findOne(['alias' => $alias]);
        if (!$attribute) {
            return false;
        }
        $assign = AttributesAssignModel::getAssign($attribute->attribute_id, $this->owner->primaryKey,
            $this->owner->getAlias());
        if (!$assign) {
            return false;
        }

        return $assign->delete();
    }

    /**
     * @return array
     */
    public function getAttrs(): array
    {
        if (empty($this->_attrs)) {
            $this->_attrs = [];

            $ownerData = $this->getAttributesForOwner();

            //print_r($ownerData); exit();

            foreach ($ownerData as $itemData) {
                $data = [
                    'attribute_id' => $itemData['attribute_id'],
                    'alias' => $itemData['attributeRelation']['attribute_id'],
                    'title' => $itemData['attributeRelation']['title'],
                    'content' => $itemData['content'],
                ];
                $this->_attrs[$itemData['attribute_id']] = $data;
            }
        }
        return $this->_attrs;
    }

    /**
     * @name array $attributes
     * @return bool
     * @throws \Exception
     */
    public function setAttrs(array $attributes): bool
    {
        foreach ($attributes as $id => $content) {
            if (!isset($this->_attrs[$id]['content'])) {
                $owner = $this->owner->getAlias();
                throw new \Exception("Undefined attribute! Owner = $owner, id = $id");
            }
            $this->_attrs[$id]['content'] = $content;
        }

        return true;
    }

    /**
     * @name array $attributes
     * @return bool
     */
    public function setAttrsArray(array $attributes): bool
    {
        $this->_attrs = $attributes;
        return true;
    }
}