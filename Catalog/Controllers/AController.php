<?php

namespace Steady\Modules\Catalog\Controllers;

use Steady\Admin\Components\TreeController;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\CategoryModel;
use yii\helpers\Inflector;
use yii\web\Response;


class AController extends TreeController
{
    public $modelName = CategoryModel::class;

    public $moduleName = 'catalog';

    public $linkRoute = '/goods';

    public $menuView = '/a/_menu';

    public $rootActions = ['fields'];

    /**
     * @inheritdoc
     * @name CategoryModel $model
     * @return void
     * @throws \ImagickException
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function beforeSave($model)
    {
        if (isset($_FILES) && $this->module->settings['categoryThumb']) {
            $this->saveImage($model);
        }

        parent::beforeSave($model);
    }

    /**
     * @inheritdoc
     */
    public function actionEdit($id)
    {
        $this->view->params['submenu'] = true;

        return parent::actionEdit($id);
    }
}