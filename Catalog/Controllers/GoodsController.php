<?php

namespace Steady\Modules\Catalog\Controllers;

use Steady\Admin\Behaviors\SortableDateControllerBehavior;
use Steady\Admin\Behaviors\StatusControllerBehavior;
use Steady\Admin\Components\AdminController;
use Steady\Engine\Helpers\Image;
use Steady\Engine\SW;
use Steady\Modules\Catalog\CatalogModule;
use Steady\Modules\Catalog\Models\BrandModel;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * @property CatalogModule $module
 * @mixin SortableDateControllerBehavior
 * @mixin StatusControllerBehavior
 */
class GoodsController extends AdminController
{
    public function behaviors()
    {
        return [
            [
                'class' => SortableDateControllerBehavior::class,
                'model' => GoodsModel::class,
            ],
            [
                'class' => StatusControllerBehavior::class,
                'model' => GoodsModel::class,
            ],
        ];
    }

    public function actionIndex($id = null)
    {
        if ($id == null) {
            $data = new ActiveDataProvider([
                'query' => GoodsModel::find()->desc(),
                'pagination' => ['pageSize' => 2000],
            ]);
            return $this->render('list', [
                'data' => $data,
            ]);
        }

        $category = CategoryModel::findOne($id);
        if (!$category) {
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        return $this->render('index', [
            'model' => $category,
        ]);
    }

    public function actionBrand($id)
    {
        $category = BrandModel::findOne($id);
        if (!$category) {
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        return $this->render('index', [
            'model' => $category,
        ]);
    }

    /**
     * @name $id
     * @return array|string|Response
     * @throws \ImagickException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function actionCreate($id)
    {
        $category = CategoryModel::findOne($id);
        if (!$category) {
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        $model = new GoodsModel();
        $model->category_id = $category->primaryKey;

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if (isset($_FILES) && $this->module->settings['itemThumb']) {
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if ($model->image && $model->validate(['image'])) {
                        $model->image = Image::upload($model->image, 'catalog');
                        if ($this->module->settings['watermark']) {
                            Image::watermark($model->image, $this->module->settings['watermark']);
                        }
                    } else {
                        $model->image = '';
                    }
                }
                if ($model->save()) {
                    $this->flash('success', SW::t('admin/catalog', 'Item created'));
                    return $this->redirect(['/admin/' . $this->module->id . '/goods/edit/', 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', SW::t('admin', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('create', [
                'form' => [
                    'model' => $model,
                    'category' => $category,
                    'categoriesValid' => CategoryModel::tree(),
                    'brandsValid' => BrandModel::tree(),
                ],
            ]);
        }
    }

    /**
     * @name $id
     * @return array|string|Response
     * @throws \ImagickException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id)
    {
        $model = GoodsModel::findOne($id);
        if (!$model) {
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if (isset($_FILES) && $this->module->settings['itemThumb']) {
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if ($model->image && $model->validate(['image'])) {
                        $model->image = Image::upload($model->image, 'catalog');
                        if ($this->module->settings['watermark']) {
                            Image::watermark($model->image, $this->module->settings['watermark']);
                        }
                    } else {
                        $model->image = $model->oldAttributes['image'];
                    }
                }

                if ($model->save()) {
                    $this->flash('success', SW::t('admin/catalog', 'Item updated'));
                    return $this->redirect(['/admin/' . $this->module->id . '/goods/edit', 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', SW::t('admin', 'Update error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('edit', [
                'form' => [
                    'model' => $model,
                    'categoriesValid' => CategoryModel::tree(),
                    'brandsValid' => BrandModel::tree(),
                ],
            ]);
        }
    }

    public function actionPhotos($id)
    {
        if (!($model = GoodsModel::findOne($id))) {
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        return $this->render('photos', [
            'model' => $model,
        ]);
    }

    public function actionOffers($id)
    {
        if (!($model = GoodsModel::findOne($id))) {
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        return $this->render('offers', [
            'goods' => $model,
        ]);
    }

    /**
     * @name $id
     * @return Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionClearImage($id)
    {
        $model = GoodsModel::findOne($id);

        if ($model === null) {
            $this->flash('error', SW::t('admin', 'Not found'));
        } else if ($model->image) {
            $model->image = '';
            if ($model->update()) {
                @unlink(SW::getAlias('@root') . $model->image);
                $this->flash('success', SW::t('admin', 'Image cleared'));
            } else {
                $this->flash('error', SW::t('admin', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }

    /**
     * @name $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (($model = GoodsModel::findOne($id))) {
            $model->delete();
        } else {
            $this->error = SW::t('admin', 'Not found');
        }
        return $this->formatResponse(SW::t('admin/catalog', 'Item deleted'));
    }

    public function actionUp($id, $category_id)
    {
        return $this->move($id, 'up', ['category_id' => $category_id]);
    }

    public function actionDown($id, $category_id)
    {
        return $this->move($id, 'down', ['category_id' => $category_id]);
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, GoodsModel::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, GoodsModel::STATUS_OFF);
    }
}