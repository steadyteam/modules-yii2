<?php

namespace Steady\Modules\Catalog\Controllers;

use Steady\Admin\Components\TreeController;
use Steady\Modules\Catalog\Models\BrandModel;

class BrandsController extends TreeController
{
    public $modelName = BrandModel::class;

    public $moduleName = 'catalog';

    public $linkController = '/brands';

    public $linkRoute = '/goods/brand';

    public $menuView = '/brands/_menu';

    /**
     * @inheritdoc
     * @name BrandModel $model
     * @return void
     * @throws \ImagickException
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function beforeSave($model)
    {
        if (isset($_FILES) && $this->module->settings['categoryThumb']) {
            $this->saveImage($model);
        }

        parent::beforeSave($model);
    }


    public function actionPhotos($id)
    {
        if (!($model = BrandModel::findOne($id))) {
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        return $this->render('photos', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     */
    protected function getRedirect()
    {
        return ["/admin/$this->moduleName/brands"];
    }
}