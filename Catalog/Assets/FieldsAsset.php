<?php

namespace Steady\Modules\Catalog\Assets;

use yii\web\AssetBundle;

class FieldsAsset extends AssetBundle
{
    public $sourcePath = '@modules/Catalog/Resources/frontend';

    public $css = [
        'css/fields.css',
    ];

    public $js = [
        'js/fields.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}