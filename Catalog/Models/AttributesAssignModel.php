<?php

namespace Steady\Modules\Catalog\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Base\Model;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use yii\helpers\ArrayHelper;

/**
 * @property int attribute_assign_id
 * @property int attribute_id
 * @property int owner_id
 * @property string class_alias
 * @property string content
 */
class AttributesAssignModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'catalog_attributes_assign';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'attribute_assign_id' => $migration->primaryKey(),
            'attribute_id' => $migration->integer(11)->notNull(),
            'owner_id' => $migration->integer(11)->notNull(),
            'class_alias' => $migration->string(128)->notNull(),
            'content' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['attribute_id', 'integer'],
            ['attribute_id', 'required'],
            ['owner_id', 'integer'],
            ['owner_id', 'required'],
            ['class_alias', SlugAdvancedValidator::class],
            ['class_alias', 'required'],
            ['content', 'string'],
            ['content', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getAttributeRelation()
    {
        return $this->hasOne(AttributeModel::class, ['attribute_id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getFilterRelation()
    {
        return $this->hasOne(FilterModel::class, ['attribute_id' => 'attribute_id', 'owner_id' => 'owner_id',
            'class_alias' => 'class_alias']);
    }

    /**
     * @name AdvancedModel $attributeModel
     * @name Model $ownerModel
     * @return bool
     * @throws \Exception
     */
    public function appendAndSave($attributeModel, $ownerModel)
    {
        $transaction = $transaction = SW::$app->db->beginTransaction();
        try {
            $saved = $attributeModel->save();
            if (!$saved) {
                throw new \Exception('AttributeModel not saved: ' . $attributeModel->formatErrors());
            }

            $this->attribute_id = $attributeModel->primaryKey;
            $this->owner_id = $ownerModel->primaryKey;
            $this->class_alias = $ownerModel->getAlias();

            $saved = $this->save();
            if (!$saved) {
                throw new \Exception(get_class($this) . ' not saved: ' . $this->formatErrors());
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * @name int $attribute_id
     * @name int $owner_id
     * @name string $class_alias
     * @return null|AttributesAssignModel
     */
    public static function getAssign(int $attribute_id, int $owner_id, string $class_alias): ?AttributesAssignModel
    {
        return AttributesAssignModel::findOne(['attribute_id' => $attribute_id, 'owner_id' => $owner_id,
            'class_alias' => $class_alias]);
    }

    /**
     * @name int $owner_id
     * @name string $class_alias
     * @return array
     */
    public static function getDataForOwner(int $owner_id, string $class_alias): array
    {
        $data = self::find()
            //->select(['*'])
            ->joinWith('attributeRelation')
            ->where([self::tableName() . '.owner_id' => $owner_id, self::tableName() . '.class_alias' => $class_alias])
            ->asArray()
            ->all();

        return $data;
    }

    /**
     * @name string $class_alias
     * @return array
     */
    public static function getDataForClass(string $class_alias): array
    {
        return self::find()
            ->select([AttributeModel::tableName() . '.*'])
            ->leftJoin(AttributeModel::tableName(), AttributeModel::tableName() . '.attribute_id = '
                . AttributesAssignModel::tableName() . '.attribute_id')
            ->where(['class_alias' => $class_alias])
            ->groupBy([AttributesAssignModel::tableName() . '.attribute_id'])
            ->asArray()
            ->all();
    }

    /**
     * @name int $attribute_id
     * @name int $owner_id
     * @name string $class_alias
     * @return AttributesAssignModel
     * @throws \yii\db\Exception
     */
    public static function create(int $attribute_id, int $owner_id, string $class_alias): AttributesAssignModel
    {
        $model = new AttributesAssignModel();
        $model->attribute_id = $attribute_id;
        $model->owner_id = $owner_id;
        $model->class_alias = $class_alias;
        $model->saveEx();
        return $model;
    }
}