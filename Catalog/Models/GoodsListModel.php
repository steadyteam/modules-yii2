<?php

namespace Steady\Engine\Modules\Field\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use yii\helpers\ArrayHelper;

/**
 * @property int goods_list_id
 * @property int user_id
 */
class GoodsListModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'catalog_goods_list';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'goods_list_id' => $migration->integer(11)->notNull(),
            'user_id' => $migration->integer(11)->null(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['user_id', 'integer'],
            ['user_id', 'required'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }
}