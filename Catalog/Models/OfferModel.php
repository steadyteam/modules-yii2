<?php

namespace Steady\Modules\Catalog\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Behaviors\SeoBehavior;
use Steady\Engine\Behaviors\TaggableBehavior;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Components\Slugable;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int offer_id
 * @property int goods_id
 * @property string slug
 * @property string title
 * @property string description
 * @property string image
 * @property string content
 * @property float price
 * @property int discount
 * @property int available
 * @property string|array data
 *
 * @mixin SeoBehavior
 * @mixin SluggableBehavior
 * @mixin TaggableBehavior
 */
class OfferModel extends AdvancedModel implements Slugable
{
    public function behaviors()
    {
        $array = [
            'seo' => SeoBehavior::class,
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => false,
            ],
            'taggabble' => TaggableBehavior::class,
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public static function tableName()
    {
        return 'catalog_offers';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'offer_id' => $migration->primaryKey(),
            'goods_id' => $migration->integer(11)->notNull(),
            'slug' => $migration->string(256)->notNull()/*->unique()*/,
            'title' => $migration->string(256)->notNull(),
            'description' => $migration->text(),
            'image' => $migration->string(512)->null(),
            'content' => $migration->text(),
            'price' => $migration->float()->null(),
            'discount' => $migration->integer()->null(),
            'available' => $migration->boolean()->defaultValue(true),
            'data' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['goods_id', 'integer'],
            ['goods_id', 'required'],
            ['slug', SlugAdvancedValidator::class],
            //['slug', 'unique'],
            ['title', 'required'],
            ['title', 'string', 'max' => 256],
            ['description', 'string'],
            ['image', 'image'],
            ['content', 'string'],
            ['price', 'number'],
            ['discount', 'integer', 'max' => 99],
            ['available', 'integer'],
            ['data', 'safe'],
            [['title', 'description', 'content'], 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'goods_id' => SW::t('admin/catalog', 'Goods'),
            'slug' => SW::t('admin', 'Slug'),
            'title' => SW::t('admin', 'Title'),
            'description' => SW::t('admin', 'Description'),
            'image' => SW::t('admin', 'Image'),
            'content' => SW::t('admin', 'Content'),
            'price' => SW::t('admin/catalog', 'Price'),
            'discount' => SW::t('admin/catalog', 'Discount'),
            'available' => SW::t('admin/catalog', 'Available'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    protected function getGoodsRelation()
    {
        return $this->hasOne(GoodsModel::class, ['goods_id' => 'goods_id']);
    }
}