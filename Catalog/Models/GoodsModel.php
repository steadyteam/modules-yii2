<?php

namespace Steady\Modules\Catalog\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Behaviors\SeoBehavior;
use Steady\Engine\Behaviors\TaggableBehavior;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Components\Slugable;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use Steady\Modules\Catalog\Behaviors\AttributableBehavior;
use Steady\Modules\Catalog\Interfaces\Attributable;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int goods_id
 * @property int category_id
 * @property int brand_id
 * @property string slug
 * @property string title
 * @property string description
 * @property string image
 * @property string content
 * @property float price
 * @property int discount
 * @property int available
 * @property string|array data
 * @property int time
 *
 * @property CategoryModel categoryRelation
 * @property BrandModel brandRelation
 *
 * @mixin AttributableBehavior
 * @mixin SeoBehavior
 * @mixin SluggableBehavior
 * @mixin TaggableBehavior
 */
class GoodsModel extends AdvancedModel implements Slugable, Attributable
{
    public function behaviors()
    {
        $array = [
            'attributable' => AttributableBehavior::class,
            'seo' => SeoBehavior::class,
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => false,
            ],
            'taggabble' => TaggableBehavior::class,
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public static function tableName()
    {
        return 'catalog_goods';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'goods_id' => $migration->primaryKey(),
            'category_id' => $migration->integer(11)->notNull(),
            'brand_id' => $migration->integer(11)->null(),
            'slug' => $migration->string(256)->notNull()->unique(),
            'available' => $migration->boolean()->defaultValue(true),
            'title' => $migration->string(256)->notNull(),
            'description' => $migration->text(),
            'image' => $migration->string(512)->null(),
            'preview' => $migration->text(),
            'content' => $migration->text(),
            'price' => $migration->float()->null(),
            'discount' => $migration->integer()->null(),
            'data' => $migration->text(),
            'time' => $migration->integer(11),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['category_id', 'integer'],
            ['category_id', 'required'],
            ['brand_id', 'integer'],
            ['slug', SlugAdvancedValidator::class],
            ['slug', 'unique'],
            ['title', 'required'],
            ['title', 'string', 'max' => 256],
            ['description', 'string'],
            ['image', 'image'],
            ['preview', 'string'],
            ['content', 'string'],
            ['price', 'number'],
            ['discount', 'integer', 'max' => 99],
            ['available', 'integer'],
            ['data', 'safe'],
            ['time', 'integer'],
            ['time', 'default', 'value' => time()],
            [['title', 'description', 'preview', 'content'], 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'category_id' => SW::t('admin/catalog', 'Category'),
            'brand_id' => SW::t('admin/catalog', 'Brand'),
            'slug' => SW::t('admin', 'Slug'),
            'title' => SW::t('admin', 'Title'),
            'description' => SW::t('admin', 'Description'),
            'image' => SW::t('admin', 'Image'),
            'preview' => SW::t('admin', 'Preview'),
            'content' => SW::t('admin', 'Content'),
            'price' => SW::t('admin/catalog', 'Price'),
            'discount' => SW::t('admin/catalog', 'Discount'),
            'available' => SW::t('admin/catalog', 'Available'),
            'time' => SW::t('admin', 'Date'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    public function beforeSave($insert)
    {
        $this->categoryRelation->goods_count = null;
        $this->categoryRelation->save();

        $this->brandRelation->goods_count = null;
        $this->brandRelation->save();

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getCategoryRelation()
    {
        return $this->hasOne(CategoryModel::class, ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getBrandRelation()
    {
        return $this->hasOne(BrandModel::class, ['brand_id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getOffersRelation()
    {
        return $this->hasMany(OfferModel::class, ['goods_id' => 'goods_id']);
    }

    /**
     * @return array
     */
    public function attrs()
    {
        return $this->attrs;
    }

    /**
     * @return OfferModel[]
     */
    public function offers()
    {
        return $this->getOffersRelation()->all();
    }

    /**
     * @return float|int|null
     */
    public function getPrice()
    {
        if ($this->price == 0) {
            $offers = $this->offers();
            $price = null;
            foreach ($offers as $offer) {
                if (!$price || ($offer->price > 0 && $price > $offer->price)) {
                    $price = $offer->price;
                }
            }
            $this->price = $price;
            $this->save();
        } else {
            $price = $this->price;
        }

        if (!$price) {
            $price = null;
        }

        return $price;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->getCategoryRelation();
    }
}