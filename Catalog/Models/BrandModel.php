<?php

namespace Steady\Modules\Catalog\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Base\Query;
use Steady\Engine\Behaviors\SeoBehavior;
use Steady\Modules\Catalog\Interfaces\Goodsable;
use Steady\Engine\Components\Slugable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\Helpers\CacheHelper;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use Steady\Modules\Catalog\Behaviors\AttributableBehavior;
use Steady\Modules\Catalog\Database\CatalogDataBase;
use Steady\Modules\Catalog\Database\GoodsSearch;
use Steady\Modules\Catalog\Interfaces\Attributable;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int brand_id
 * @property string slug
 * @property string title
 * @property string image
 * @property string preview
 * @property string content
 * @property int $goods_count
 *
 * @mixin AttributableBehavior
 * @mixin SeoBehavior
 * @mixin SluggableBehavior
 */
class BrandModel extends TreeModel implements Goodsable, Slugable, Attributable
{
    public static function tableName()
    {
        return 'catalog_brands';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'brand_id' => $migration->primaryKey(),
            'slug' => $migration->string(256)->notNull()->unique(),
            'title' => $migration->string(256)->notNull(),
            'image' => $migration->string(512)->null(),
            'preview' => $migration->text(),
            'content' => $migration->text(),
            'goods_count' => $migration->integer(11)->null(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new BrandModel();
        $model->slug = 'base';
        $model->type = 1;
        $model->title = 'Base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }

    public function behaviors()
    {
        $array = [
            'attributable' => AttributableBehavior::class,
            'seo' => SeoBehavior::class,
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => false,
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();
    }

    public function rules()
    {
        $array = [
            ['slug', SlugAdvancedValidator::class],
            ['slug', 'unique'],
            ['title', 'string', 'max' => 256],
            ['title', 'required'],
            ['title', 'trim'],
            ['image', 'image'],
            ['preview', 'string'],
            ['preview', 'trim'],
            ['content', 'string'],
            ['content', 'trim'],
            ['goods_count', 'integer'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'parent_id' => SW::t('admin/catalog', 'Parent brand'),
            'slug' => SW::t('admin', 'Slug'),
            'title' => SW::t('admin', 'Title'),
            'image' => SW::t('admin', 'Image'),
            'preview' => SW::t('admin', 'Preview'),
            'content' => SW::t('admin', 'Content'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getGoodsRelation()
    {
        return $this->hasMany(GoodsModel::class, ['brand_id' => 'brand_id']);
    }

    /**
     * @return int
     */
    public function goodsCount()
    {
        if (!$this->hasAttribute('goods_count')) {
            $count = $this->goods(true)->count();
        } else if ($this->goods_count === null) {
            $count = $this->goods(true)->count();
            $this->goods_count = $count;
            $this->save();
        } else {
            $count = $this->goods_count;
        }
        return $count;
    }

    /**
     * @name bool $returnQuery
     * @name null $depth
     * @return GoodsModel[]|Query
     */
    public function goods($returnQuery = false, $depth = null)
    {
        $search = new GoodsSearch();
        $query = $search->brand([$this->brand_id])->getQuery();

        return $returnQuery ? $query : $query->all();
    }

    /**
     * @return CategoryModel[]
     */
    public function categories()
    {
        $brandIds = $this->getDescendantsIds(null, true);
        $brandIds[] = $this->brand_id;

        $categoriesIds = CatalogDataBase::getCategoriesIdsByBrandsIds($brandIds);

        return CategoryModel::findAll(['category_id' => $categoriesIds]);
    }

    /**
     * @name bool $filter
     * @return array
     */
    public function attrs($filter = true)
    {
        return CacheHelper::cache("brand_attrs_$this->brand_id", 3600, function ($filter) {
            $search = new GoodsSearch();
            $query = $search->brand([$this->brand_id])->getQuery();
            $goodsIds = $query->select('goods_id')->column();

            $query = AttributesAssignModel::find()
                ->from(['aa' => AttributesAssignModel::tableName()])
                ->select(['aa.attribute_id', 'aa.content'])
                ->distinct();

            if ($filter) {
                $query->innerJoin(FilterModel::tableName() . ' f', "f.attribute_id = aa.attribute_id and f.owner_id = "
                    . $this->brand_id . " and f.class_alias = '" . $this->getAlias() . "'");
            }

            $attrsAssigns = $query->where(['aa.class_alias' => GoodsModel::getClassAlias(), 'aa.owner_id' => $goodsIds])
                ->orderBy('aa.content')
                ->asArray()
                ->all();

            $goodsData = [];
            foreach ($attrsAssigns as $data) {
                $goodsData[$data['attribute_id']][] = $data['content'];
            }

            $attrs = [];
            foreach ($this->attrs as $id => $data) {
                if (isset($goodsData[$id])) {
                    $attrs[$id] = $data;
                    $attrs[$id]['values'] = $goodsData[$id] ?? null;
                }
            }

            return $attrs;
        });
    }

    /**
     * @return array
     */
    public function priceRange()
    {
        return CacheHelper::cache("brand_price_range_$this->brand_id", 3600, function () {
            return CatalogDataBase::getPriceRangeByModel($this);
        });
    }
}