<?php

namespace Steady\Modules\Catalog\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Base\Query;
use Steady\Engine\Behaviors\SeoBehavior;
use Steady\Modules\Catalog\Interfaces\Goodsable;
use Steady\Engine\Components\Slugable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\Helpers\CacheHelper;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugAdvancedValidator;
use Steady\Modules\Catalog\Behaviors\AttributableBehavior;
use Steady\Modules\Catalog\Database\CatalogDataBase;
use Steady\Modules\Catalog\Database\GoodsSearch;
use Steady\Modules\Catalog\Interfaces\Attributable;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int category_id
 * @property string slug
 * @property int brand_id
 * @property string title
 * @property string image
 * @property string preview
 * @property string content
 * @property int $goods_count
 *
 * @mixin AttributableBehavior
 * @mixin SeoBehavior
 * @mixin SluggableBehavior
 */
class CategoryModel extends TreeModel implements Goodsable, Slugable, Attributable
{
    public static $fieldTypes = [
        'string' => 'String',
        'text' => 'Text',
        'boolean' => 'Boolean',
        'select' => 'SelectWidget',
        'checkbox' => 'Checkbox',
    ];

    public function behaviors()
    {
        $array = [
            'attributable' => AttributableBehavior::class,
            'seo' => SeoBehavior::class,
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => false,
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->brand_id = 1;
        }
    }

    public static function tableName()
    {
        return 'catalog_categories';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'category_id' => $migration->primaryKey(),
            'slug' => $migration->string(256)->notNull()->unique(),
            'brand_id' => $migration->integer(11)->null(),
            'title' => $migration->string(256)->notNull(),
            'image' => $migration->string(512)->null(),
            'preview' => $migration->text(),
            'content' => $migration->text(),
            'goods_count' => $migration->integer(11)->null(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['slug', SlugAdvancedValidator::class],
            ['slug', 'unique'],
            ['brand_id', 'integer'],
            ['title', 'string', 'max' => 256],
            ['title', 'required'],
            ['title', 'trim'],
            ['image', 'image'],
            ['preview', 'string'],
            ['preview', 'trim'],
            ['content', 'string'],
            ['content', 'trim'],
            ['goods_count', 'integer'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'parent_id' => SW::t('admin/catalog', 'Parent category'),
            'slug' => SW::t('admin', 'Slug'),
            'brand_id' => SW::t('admin', 'Brand'),
            'title' => SW::t('admin', 'Title'),
            'image' => SW::t('admin', 'Image'),
            'preview' => SW::t('admin', 'Preview'),
            'content' => SW::t('admin', 'Content'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new CategoryModel();
        $model->slug = 'base';
        $model->title = 'Base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getGoodsRelation()
    {
        return $this->hasMany(GoodsModel::class, ['category_id' => 'category_id']);
    }

    /**
     * @return bool|void
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterDelete()
    {
        return false;

        // TODO: Переделать, удалять все товары категории, возможно не стоит
        parent::afterDelete();

        /** @var GoodsModel[] $items */
        $items = $this->getItems()->all();

        foreach ($items as $item) {
            $item->delete();
        }
    }

    /**
     * @name null $depth
     * @name bool $returnQuery
     * @return GoodsModel[]|Query
     */
    public function goods($returnQuery = false, $depth = null)
    {
        $search = new GoodsSearch();
        $query = $search->category([$this->category_id], $depth)->getQuery();

        return $returnQuery ? $query : $query->all();
    }

    /**
     * @return int
     */
    public function goodsCount()
    {
        if (!$this->hasAttribute('goods_count')) {
            $count = $this->goods(true)->count();
        } else if ($this->goods_count === null) {
            $count = $this->goods(true)->count();
            $this->goods_count = $count;
            $this->save();
        } else {
            $count = $this->goods_count;
        }
        return $count;
    }

    /**
     * @return BrandModel[]
     */
    public function brands()
    {
        $categoryIds = $this->getDescendantsIds(null, true);
        $categoryIds[] = $this->category_id;

        return CatalogDataBase::getBrandsByCategories($categoryIds);
    }

    /**
     * @param bool $filter
     * @param array $searchParams
     * @name null $depth
     * @return array
     */
    public function attrs($filter = true, $searchParams = [], $depth = null)
    {
        //return CacheHelper::cache("cat_attrs_$this->category_id", 3600, function () use ($depth, $searchParams, $filter) {
        $search = new GoodsSearch(true);
        $query = $search
            ->category([$this->category_id], $depth)
            ->price($searchParams['minPrice'] ?? 0, $searchParams['maxPrice'] ?? null)
            ->attributes($searchParams['attributes'] ?? [])
            ->available($searchParams['available'] ?? null)
            ->getQuery();
        $goodsIds = $query->select('goods_id')->column();

        // Получение значения аттрибутов, которые есть у товаров, с учетом включенных фильтров у категории
        $query = AttributesAssignModel::find()
            ->from(['aa' => AttributesAssignModel::tableName()])
            ->select(['aa.attribute_id', 'aa.content', 'COUNT(*) AS count']);
        //->distinct();
        if ($filter) {
            $query->innerJoin(FilterModel::tableName() . ' f', "f.attribute_id = aa.attribute_id and f.owner_id = "
                . $this->category_id . " and f.class_alias = '" . $this->getAlias() . "'");
        }
        $attrsAssigns = $query->where(['aa.class_alias' => GoodsModel::getClassAlias(), 'aa.owner_id' => $goodsIds])
            ->groupBy(['aa.content', 'aa.content'])
            ->orderBy('aa.content')
            ->asArray()
            //->indexBy('attribute_id')
            ->all();

        // Формирование массива значений аттрибутов
        $goodsData = [];
        foreach ($attrsAssigns as $data) {
            $goodsData[$data['attribute_id']][$data['content']] = $data;
        }

        // Получение всех атрибутов категории и составление итогового массива аттрибутов, которые есть у товаров
        $attrs = [];
        foreach ($this->attrs as $id => $data) {
            if (isset($goodsData[$id])) {
                $attrs[$id] = $data;
                $attrs[$id]['values'] = $goodsData[$id] ?? null;
            }
        }

        //print_r($attrs); exit();

        return $attrs;
        //});
    }

    /**
     * @return array
     */
    public function priceRange()
    {
        return CacheHelper::cache("cat_price_range_$this->category_id", 3600, function () {
            return CatalogDataBase::getPriceRangeByModel($this);
        });
    }

    public function getForFilterAttrs($searchParams = [])
    {
        $current = $this->attrs(true, $searchParams);

        $search = new GoodsSearch(true);
        $query = $search
            ->category([$this->category_id])
            ->price($searchParams['minPrice'] ?? 0, $searchParams['maxPrice'] ?? null)
            //->attributes($searchParams['attributes'] ?? [])
            ->available($searchParams['available'] ?? null)
            ->getQuery();
        $goodsIds = $query->select('goods_id')->column();

        // Получение значения аттрибутов, которые есть у товаров, с учетом включенных фильтров у категории
        $attrsAssigns = AttributesAssignModel::find()
            ->from(['aa' => AttributesAssignModel::tableName()])
            ->select(['aa.attribute_id', 'aa.content', 'COUNT(*) AS count'])
            ->innerJoin(FilterModel::tableName() . ' f', "f.attribute_id = aa.attribute_id and f.owner_id = "
                . $this->category_id . " and f.class_alias = '" . $this->getAlias() . "'")
            ->where(['aa.class_alias' => GoodsModel::getClassAlias(), 'aa.owner_id' => $goodsIds])
            ->groupBy(['aa.content', 'aa.content'])
            ->orderBy('aa.content')
            ->asArray()
            ->all();

        // Формирование массива значений аттрибутов
        $goodsData = [];
        foreach ($attrsAssigns as $data) {
            $goodsData[$data['attribute_id']][$data['content']] = $data;
        }

        foreach ($goodsData as $data) {

        }

        return $current;
    }
}