<?php

namespace Steady\Modules\Catalog\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int attribute_id
 * @property string alias
 * @property boolean folder
 * @property boolean required
 * @property string title
 * @property string content
 * @property int frequency
 *
 * @mixin SluggableBehavior
 */
class AttributeModel extends TreeModel implements Aliasable
{
    const TYPE_STRING = 1;
    const TYPE_INTEGER = 2;
    const TYPE_FLOAT = 3;
    const TYPE_BOOLEAN = 5;
    const TYPE_ARRAY = 7;
    const TYPE_INTERVAL = 9;
    const TYPE_DICTIONARY = 11;
    const TYPE_TAGS = 15;

    public function behaviors()
    {
        $array = [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'slugAttribute' => 'alias',
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => false,
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->folder = false;
            $this->required = false;
        }
    }

    public static function tableName()
    {
        return 'catalog_attributes';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'attribute_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'folder' => $migration->boolean()->notNull()->defaultValue(false),
            'required' => $migration->boolean()->notNull()->defaultValue(false),
            'title' => $migration->string(256)->notNull(),
            'content' => $migration->text(),
            'frequency' => $migration->integer(11)->null(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['folder', 'boolean'],
            ['folder', 'default', 'value' => false],
            ['required', 'boolean'],
            ['required', 'default', 'value' => false],
            ['title', 'required'],
            ['title', 'trim'],
            ['title', 'string', 'max' => 128],
            ['content', 'string'],
            ['content', 'trim'],
            ['frequency', 'integer'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'alias' => SW::t('admin', 'Alias'),
            'required' => SW::t('admin', 'Required'),
            'title' => SW::t('admin', 'Title'),
            'content' => SW::t('admin', 'Content'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getAssignRelation()
    {
        return $this->hasMany(AttributesAssignModel::class, ['attribute_id' => 'attribute_id']);
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new AttributeModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->folder = true;
        $model->required = false;
        $model->title = 'Base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }

    /**
     * @name string $alias
     * @name string $title
     * @name int $type
     * @return AttributeModel
     * @throws \yii\db\Exception
     */
    public static function create(string $alias, string $title, int $type): AttributeModel
    {
        $model = new AttributeModel();
        $model->alias = $alias;
        $model->title = $title;
        $model->type = $type;
        $model->parent_id = 1;
        $model->saveEx();
        return $model;
    }

}