<?php

namespace Steady\Modules\Catalog\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Validators\SlugAdvancedValidator;
use yii\helpers\ArrayHelper;

/**
 * @property int filter_id
 * @property int attribute_id
 * @property int owner_id
 * @property string class_alias
 * @property string content
 */
class FilterModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'catalog_filters';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'filter_id' => $migration->primaryKey(),
            'attribute_id' => $migration->integer(11)->notNull(),
            'owner_id' => $migration->integer(11)->notNull(),
            'class_alias' => $migration->string(128)->notNull(),
            'content' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['attribute_id', 'integer'],
            ['attribute_id', 'required'],
            ['owner_id', 'integer'],
            ['owner_id', 'required'],
            ['class_alias', SlugAdvancedValidator::class],
            ['class_alias', 'required'],
            ['content', 'string'],
            ['content', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getAttributeRelation()
    {
        return $this->hasOne(AttributeModel::class, ['attribute_id' => 'attribute_id']);
    }

    /**
     * @name int $attribute_id
     * @name int $owner_id
     * @name string $class_alias
     * @return null|AttributesAssignModel
     */
    public static function getFilter(int $attribute_id, int $owner_id, string $class_alias): ?FilterModel
    {
        return FilterModel::findOne(['attribute_id' => $attribute_id, 'owner_id' => $owner_id,
            'class_alias' => $class_alias]);
    }

    /**
     * @name int $owner_id
     * @name string $class_alias
     * @return array
     */
    public static function getDataForOwner(int $owner_id, string $class_alias): array
    {
        return self::find()
            ->joinWith('attributeRelation')
            ->where(['owner_id' => $owner_id, 'class_alias' => $class_alias])
            ->asArray()
            ->all();
    }

    /**
     * @name int $attribute_id
     * @name int $owner_id
     * @name string $class_alias
     * @return FilterModel
     * @throws \yii\db\Exception
     */
    public static function create(int $attribute_id, int $owner_id, string $class_alias): FilterModel
    {
        $model = new FilterModel();
        $model->attribute_id = $attribute_id;
        $model->owner_id = $owner_id;
        $model->class_alias = $class_alias;
        $model->saveEx();
        return $model;
    }
}