<?php

namespace Steady\Engine\Modules\Field\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use yii\helpers\ArrayHelper;

/**
 * @property int goods_assign_id
 * @property int goods_list_id
 * @property int goods_id
 * @property float quantity
 */
class GoodsAssignModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'catalog_goods_assign';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'goods_assign_id' => $migration->primaryKey(),
            'goods_list_id' => $migration->integer(11)->notNull(),
            'goods_id' => $migration->integer(11)->notNull(),
            'quantity' => $migration->float(11)->notNull(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['goods_list_id', 'integer'],
            ['goods_list_id', 'required'],
            ['goods_id', 'integer'],
            ['goods_id', 'required'],
            ['quantity', 'number'],
            ['quantity', 'required'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @name int $goodsListId
     * @name int $goodsId
     * @name string $quantity
     * @return GoodsAssignModel
     * @throws \yii\db\Exception
     */
    public static function add(int $goodsListId, int $goodsId, string $quantity): GoodsAssignModel
    {
        $model = new GoodsAssignModel();
        $model->goods_list_id = $goodsListId;
        $model->goods_id = $goodsId;
        $model->quantity = $quantity;
        $model->saveEx();
        return $model;
    }

    /**
     * @name int $goodsListId
     * @name int $goodsId
     * @return null|GoodsAssignModel
     */
    public static function getAssign(int $goodsListId, int $goodsId): ?GoodsAssignModel
    {
        return GoodsAssignModel::findOne(['goods_list_id' => $goodsListId, 'goods_id' => $goodsId]);
    }
}