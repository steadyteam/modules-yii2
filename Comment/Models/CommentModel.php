<?php

namespace Steady\Modules\Comment\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\SW;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int comment_id
 * @property int user_id
 * @property string content
 * @property int like
 * @property int dislike
 * @property int frequency
 * @property array data
 *
 * @mixin SluggableBehavior
 */
class CommentModel extends TreeModel
{
    public function behaviors()
    {
        $array = [
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();
    }

    public static function tableName()
    {
        return 'comments';
    }

    public function rules()
    {
        $array = [
            ['user_id', 'integer'],
            ['content', 'required'],
            ['content', 'string'],
            ['content', 'trim'],
            ['like', 'integer'],
            ['dislike', 'integer'],
            ['frequency', 'integer'],
            ['data', 'safe'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'comment_id' => $migration->primaryKey(),
            'user_id' => $migration->integer(11)->null(),
            'content' => $migration->text(),
            'like' => $migration->integer(11)->null(),
            'dislike' => $migration->integer(11)->null(),
            'frequency' => $migration->integer(11)->null(),
            'data' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new CommentModel();
        $model->content = 'base';
        $model->type = 0;
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }

    /**
     * @param string $content
     * @param int|null $type
     * @param int|null $userId
     * @param array $params
     * @return CommentModel
     * @throws \yii\db\Exception
     */
    public static function create(string $content, ?int $type = 1, ?int $userId = null, $params = []): CommentModel
    {
        $model = new CommentModel();
        $model->content = $content;
        $model->user_id = $userId ?? SW::$app->user->getId();
        $model->params = $params;
        $model->type = $type;
        $model->parent_id = 1;
        $model->saveEx();
        return $model;
    }
}