<?php

namespace Steady\Modules\Comment\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Validators\SlugAdvancedValidator;
use yii\helpers\ArrayHelper;

/**
 * @property int comment_assign_id
 * @property int comment_id
 * @property int owner_id
 * @property string class_alias
 * @property array data
 */
class CommentAssignModel extends AdvancedModel
{

    public static function tableName()
    {
        return 'comments_assign';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'comment_assign_id' => $migration->primaryKey(),
            'comment_id' => $migration->integer(11)->notNull(),
            'owner_id' => $migration->integer(11)->notNull(),
            'class_alias' => $migration->string(128)->notNull(),
            'data' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['comment_id', 'integer'],
            ['comment_id', 'required'],
            ['owner_id', 'integer'],
            ['owner_id', 'required'],
            ['class_alias', SlugAdvancedValidator::class],
            ['class_alias', 'required'],
            ['data', 'safe'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @name int $comment_id
     * @name int $owner_id
     * @name string $class_alias
     * @return CommentAssignModel
     * @throws \yii\db\Exception
     */
    public static function create(int $comment_id, int $owner_id, string $class_alias): CommentAssignModel
    {
        $model = new CommentAssignModel();
        $model->comment_id = $comment_id;
        $model->owner_id = $owner_id;
        $model->class_alias = $class_alias;
        $model->saveEx();
        return $model;
    }

    /**
     * @name int $comment_id
     * @name int $owner_id
     * @name string $class_alias
     * @return null|CommentAssignModel
     */
    public static function getAssign(int $comment_id, int $owner_id, string $class_alias): ?CommentAssignModel
    {
        return CommentAssignModel::findOne(['comment_id' => $comment_id, 'owner_id' => $owner_id,
            'class_alias' => $class_alias]);
    }

    /**
     * @name string $class_alias
     * @return array
     */
    public static function getDataForClass(string $class_alias): array
    {
        return self::find()
            ->select([CommentModel::tableName() . '.*'])
            ->leftJoin(CommentModel::tableName(), CommentModel::tableName() . '.comment_id = '
                . CommentAssignModel::tableName() . '.comment_id')
            ->where(['class_alias' => $class_alias])
            ->groupBy([CommentAssignModel::tableName() . '.comment_id'])
            ->asArray()
            ->all();
    }
}