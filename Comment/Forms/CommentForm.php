<?php

namespace Steady\Modules\Comment\Forms;

use Steady\Engine\Base\Form;

class CommentForm extends Form
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $text;

    public function rules()
    {
        return [
            ['name', 'string'],
            ['name', 'required'],
            ['text', 'string'],
            ['text', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'text' => 'Комментарий',
        ];
    }
}