<?php

namespace Steady\Modules\Comment;

use Steady\Engine\Base\Module;

class CommentModule extends Module
{
    public static $installConfig = [
        'name' => 'comment',
        'class' => self::class,
        'title' => [
            'en' => 'Comments',
            'ru' => 'Комментарии',
        ],
        'icon' => 'comments',
        'order_num' => 45,
    ];

    public $settings = [
    ];
}