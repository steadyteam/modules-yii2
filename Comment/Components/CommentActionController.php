<?php

namespace Steady\Modules\Comment\Components;

use Steady\Engine\Base\Controller;
use Steady\Engine\SW;
use Steady\Modules\Comment\Forms\CommentForm;
use Steady\Modules\Comment\Models\CommentModel;
use yii\web\Response;

class CommentActionController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        // TODO: Временное рещение, разобраться c CSRF
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * @name $id
     * @return array|Response
     * @throws \Exception
     */
    public function actionAdd()
    {
        $form = new CommentForm();

        if (!$form->load(SW::$app->request->post()) || !$form->validate()) {
            return ['success' => false, 'error' => $form->errors];
        }

        $data = [
            'name' => $form->name,
        ];

        $result = CommentModel::create($form->text, 1, SW::$app->user->getId(), $data);

        return ['success' => $result, 'error' => $result ? null : 'Не удалось отправить комментарий'];
    }

    /**
     * @name $id
     * @return array|Response
     * @throws \Exception
     */
    public function actionRemove($id)
    {
    }

    /**
     * @name $id
     * @return array
     * @throws \Exception
     */
    public function actionPlus($id)
    {
    }

    /**
     * @name $id
     * @return array
     * @throws \Exception
     */
    public function actionMinus($id)
    {
    }
}