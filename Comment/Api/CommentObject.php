<?php

namespace Steady\Modules\Comment\Api;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\Components\TreeObjectInterface;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Modules\Comment\Models\CommentModel;

/**
 * @property CommentModel $model
 */
class CommentObject extends ApiObject implements TreeObjectInterface
{
    use TreeObjectTrait;

    public $modelName = CommentModel::class;

    public $comment_id;
    public $user_id;
    public $content;
    public $like;
    public $dislike;
}