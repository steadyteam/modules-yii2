<?php

namespace Steady\Modules\Comment\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\Components\TreeApiTrait;
use Steady\Modules\Comment\CommentModule;
use Steady\Modules\Comment\Models\CommentModel;

class CommentApi extends Api
{
    use TreeApiTrait;

    public $moduleName = CommentModule::class;

    public $modelName = CommentModel::class;

    public $objectName = CommentObject::class;
}