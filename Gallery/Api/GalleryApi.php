<?php

namespace Steady\Modules\Gallery\Api;

use Steady\Admin\Api\PhotoObject;
use Steady\Engine\Base\Api;
use Steady\Engine\Components\ItemsApiTrait;
use Steady\Engine\Components\TreeApiTrait;
use Steady\Engine\Models\PhotoModel;
use Steady\Modules\Gallery\GalleryModule;
use Steady\Modules\Gallery\Models\GalleryModel;

class GalleryApi extends Api
{
    use TreeApiTrait;
    use ItemsApiTrait;

    public $moduleName = GalleryModule::class;

    public $modelName = GalleryModel::class;

    public $objectName = GalleryObject::class;

    public $itemModelName = PhotoModel::class;

    public $itemObjectName = PhotoObject::class;
}