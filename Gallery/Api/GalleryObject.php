<?php

namespace Steady\Modules\Gallery\Api;

use Steady\Admin\Api\PhotoObject;
use Steady\Engine\Base\ApiObject;
use Steady\Engine\Components\ItemsObjectTrait;
use Steady\Engine\Components\TreeObjectInterface;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Engine\Models\PhotoModel;
use Steady\Modules\Gallery\Models\GalleryModel;

/**
 * @property GalleryModel $model
 */
class GalleryObject extends ApiObject implements TreeObjectInterface
{
    use TreeObjectTrait;
    use ItemsObjectTrait;

    public $modelName = GalleryModel::class;

    public $itemModelName = PhotoModel::class;

    public $itemObjectName = PhotoObject::class;

    public $gallery_id;
    public $alias;
    public $title;
}