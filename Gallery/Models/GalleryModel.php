<?php

namespace Steady\Modules\Gallery\Models;


use Steady\Engine\Base\Migration;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int gallery_id
 * @property string alias
 * @property string title
 * @property string image
 * @property string content
 *
 * @mixin SluggableBehavior
 */
class GalleryModel extends TreeModel implements Aliasable
{

    public function behaviors()
    {
        $array = [
            'sluggable' => [
                'slugAttribute' => 'alias',
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => false,
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public static function tableName()
    {
        return 'gallery_categories';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'gallery_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'title' => $migration->string(256)->notNull(),
            'image' => $migration->string(512)->null(),
            'content' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['title', 'string', 'max' => 128],
            ['title', 'required'],
            ['title', 'trim'],
            ['image', 'image'],
            ['content', 'string'],
            ['content', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'parent_id' => SW::t('admin', 'Parent gallery'),
            'alias' => SW::t('admin', 'Alias'),
            'title' => SW::t('admin', 'Title'),
            'image' => SW::t('admin', 'Image'),
            'content' => SW::t('admin', 'Content'),
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new GalleryModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->title = 'Base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }
}