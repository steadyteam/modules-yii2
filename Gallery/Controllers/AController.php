<?php

namespace Steady\Modules\Gallery\Controllers;

use Steady\Admin\Components\TreeController;
use Steady\Modules\Gallery\Models\GalleryModel;

class AController extends TreeController
{
    public $modelName = GalleryModel::class;

    public $moduleName = 'gallery';

    public $linkRoute = '/a/photos';

    public $menuView = '/a/_menu';

    /**
     * @name $id
     * @return string|\yii\web\Response
     */
    public function actionPhotos($id)
    {
        $model = GalleryModel::findOne($id);

        if (!$model) {
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        return $this->render('photos', [
            'model' => $model,
        ]);
    }
}