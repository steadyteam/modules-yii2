<?php

namespace Steady\Modules\Gallery;

use Steady\Admin\Components\SteadyModule;

class GalleryModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'gallery',
        'class' => self::class,
        'title' => [
            'en' => 'Photo Gallery',
            'ru' => 'Фотогалерея',
        ],
        'icon' => 'picture',
        'order_num' => 70,
    ];
    public $settings = [
        'categoryThumb' => true,
        'itemsInFolder' => false,
    ];
}