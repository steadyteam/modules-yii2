<?php

use Steady\Engine\SW;

$this->title = SW::t('admin', 'Create category');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>