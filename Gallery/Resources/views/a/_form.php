<?php

use Steady\Engine\SW;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$class = $this->context->modelName;
$settings = $this->context->module->settings;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'alias') ?>
<?= $form->field($model, 'parent_id')->dropDownList(
    ArrayHelper::map($validParents, 'gallery_id', 'title'),
    ['prompt' => '- SelectWidget category -']) ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-success btn-c-md']) ?>
<?php ActiveForm::end(); ?>