<?php

use Steady\Admin\Widgets\PhotosWidget;

$this->title = $model->title;

?>

<?= $this->render('_menu') ?>

<?= PhotosWidget::widget(['model' => $model]) ?>