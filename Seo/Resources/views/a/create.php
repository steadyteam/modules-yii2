<?php

use Steady\Engine\SW;

$this->title = SW::t('admin', 'Create');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>