<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$this->title = SW::t('admin', 'Redirects');

$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<?php if ($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= SW::t('admin', 'From') ?></th>
            <th><?= SW::t('admin', 'To') ?></th>
            <th><?= SW::t('admin', 'Status') ?></th>
            <th width="30"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $item) : ?>
            <tr>
                <td>
                    <a href="<?= Url::to(['/admin/' . $module . '/a/edit', 'id' => $item->primaryKey]) ?>">
                        <?= $item->primaryKey ?>
                    </a>
                </td>
                <td>
                    <a href="<?= Url::to($item->from, true) ?>">
                        <?= $item->from ?>
                    </a>
                </td>
                <td>
                    <a href="<?= Url::to($item->to, true) ?>">
                        <?= $item->to ?>
                    </a>
                </td>
                <td><?= $item->status ?></td>
                <td><a href="<?= Url::to(['/admin/' . $module . '/a/delete', 'id' => $item->primaryKey]) ?>"
                       class="glyphicon glyphicon-remove confirm-delete"
                       title="<?= SW::t('admin', 'Delete item') ?>"></a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination,
    ]) ?>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>