<?php

use Steady\Engine\SW;

$this->title = SW::t('admin', 'Edit');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>