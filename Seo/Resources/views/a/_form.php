<?php

use Steady\Engine\SW;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form'],
]); ?>
<?= $form->field($model, 'from') ?>
<?= $form->field($model, 'to') ?>
<?= $form->field($model, 'status') ?>
<?= $form->field($model, 'type') ?>
<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>