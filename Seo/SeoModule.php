<?php

namespace Steady\Modules\Seo;

use Steady\Engine\Base\Module;

class SeoModule extends Module
{
    public static $installConfig = [
        'name' => 'seo',
        'class' => self::class,
        'title' => [
            'en' => 'Seo',
            'ru' => 'Seo',
        ],
        'icon' => 'seo',
        'order_num' => 45,
    ];

    public $settings = [
    ];

}