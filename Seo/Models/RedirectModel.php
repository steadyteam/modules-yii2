<?php

namespace Steady\Modules\Seo\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int redirect_id
 * @property string from
 * @property string to
 *
 * @mixin SluggableBehavior
 */
class RedirectModel extends AdvancedModel
{
    public function behaviors()
    {
        $array = [
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->status = 301;
        }
    }

    public static function tableName()
    {
        return 'seo_redirects';
    }

    public function rules()
    {
        $array = [
            ['from', 'required'],
            ['from', 'string'],
            ['to', 'required'],
            ['to', 'string'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'redirect_id' => $migration->primaryKey(),
            'from' => $migration->string(512),
            'to' => $migration->string(512),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }
}