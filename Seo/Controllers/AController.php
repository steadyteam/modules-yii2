<?php

namespace Steady\Modules\Seo\Controllers;

use Steady\Admin\Components\AdminController;
use Steady\Engine\SW;
use Steady\Modules\Seo\Models\RedirectModel;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AController extends AdminController
{
    public $rootActions = 'all';

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => RedirectModel::find(),
        ]);
        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionCreate()
    {
        $model = new RedirectModel();

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', 'Success');
                    return $this->redirect(['/admin/' . $this->module->id]);
                } else {
                    $this->flash('error', SW::t('admin', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('create', [
                'form' => [
                    'model' => $model,
                ],
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = RedirectModel::findOne($id);

        if ($model === null) {
            $this->flash('error', SW::t('admin', 'Not found'));
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', 'Success');
                } else {
                    $this->flash('error', SW::t('admin', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        } else {
            return $this->render('edit', [
                'form' => [
                    'model' => $model,
                ],
            ]);
        }
    }

    /**
     * @name $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (($model = RedirectModel::findOne($id))) {
            $model->delete();
        } else {
            $this->error = SW::t('admin', 'Not found');
        }
        return $this->formatResponse('Deleted');
    }
}