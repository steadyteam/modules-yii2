<?php

namespace Steady\Modules\Navigation\Api;

use Steady\Engine\Base\Api;
use Steady\Engine\Components\ItemsApiTrait;
use Steady\Engine\Components\TreeApiTrait;
use Steady\Modules\Navigation\Models\NavigationModel;
use Steady\Modules\Navigation\NavigationModule;

class NavigationApi extends Api
{
    use TreeApiTrait;
    use ItemsApiTrait;

    public $moduleName = NavigationModule::class;

    public $modelName = NavigationModel::class;

    public $objectName = NavigationObject::class;

    public $itemModelName = NavigationModel::class;

    public $itemObjectName = NavigationObject::class;
}