<?php

namespace Steady\Modules\Navigation\Api;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\Components\ItemsObjectTrait;
use Steady\Engine\Components\TreeObjectTrait;
use Steady\Modules\Navigation\Models\NavigationModel;

/**
 * @property NavigationModel $model
 */
class NavigationObject extends ApiObject
{
    use TreeObjectTrait;
    use ItemsObjectTrait {
        items as protected traitItems;
    }

    public $modelName = NavigationModel::class;

    /**
     * @name array $options
     * @return \Steady\Engine\Base\ApiObject[]
     *
     */
    public function items($options = [])
    {
        $query = $this->model->descendants();

        $options['query'] = $query;

        return $this->traitItems($options);
    }
}