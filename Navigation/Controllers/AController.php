<?php

namespace Steady\Admin\Modules\Navigation\Controllers;

use Steady\Admin\Components\ApiController;
use Steady\Engine\SW;
use Steady\Modules\Navigation\Models\NavigationModel;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;

class AController extends ApiController
{
    public $rootActions = ['create', 'delete'];

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => NavigationModel::find()->desc(),
        ]);
        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionCreate($slug = null)
    {
        $model = new NavigationModel();

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', SW::t('admin/navigation', 'Navigation created'));
                    return $this->redirect(['/admin/' . $this->module->id]);
                } else {
                    $this->flash('error', SW::t('admin', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            if ($slug) $model->slug = $slug;

            return $this->render('create', [
                'form' => [
                    'model' => $model,
                    'pagesIds' => NavigationModel::find()->all(),
                ],
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = NavigationModel::findOne($id);

        if ($model === null) {
            $this->flash('error', SW::t('admin', 'Not found'));
            return $this->redirect(['/admin/' . $this->module->id]);
        }

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', SW::t('admin/navigation', 'Navigation updated'));
                } else {
                    $this->flash('error', SW::t('admin', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        } else {
            return $this->render('edit', [
                'form' => [
                    'model' => $model,
                    'pagesIds' => NavigationModel::find()->all(),
                ],
            ]);
        }
    }

    /**
     * @name $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (($model = NavigationModel::findOne($id))) {
            $model->delete();
        } else {
            $this->error = SW::t('admin', 'Not found');
        }
        return $this->responseFormat(SW::t('admin/navigation', 'Navigation deleted'));
    }

    /**
     * @throws \Exception
     */
    public function actionUpdateTree()
    {
        NavigationModel::buildTree();

        return 'Success!';
    }
}