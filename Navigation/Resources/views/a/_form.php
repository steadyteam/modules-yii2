<?php

use Steady\Admin\Widgets\RedactorWidget;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form'],
]); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'layout_id')->dropDownList(
    ArrayHelper::map($layoutsIds, 'layout_id', 'name'),
    ['prompt' => '- SelectWidget layout -']) ?>
<?= $form->field($model, 'parent_id')->dropDownList(
    ArrayHelper::map($pagesIds, 'navigation_id', 'title'),
    ['prompt' => '- SelectWidget page -']) ?>
<?= $form->field($model, 'type') ?>
<?= $form->field($model, 'slug') ?>
<?= $form->field($model, 'content')->widget(RedactorWidget::className(), [
    'options' => [
        'minHeight' => 100,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
    ],
]) ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>