<?php

use Steady\Engine\SW;

$this->title = SW::t('admin/page', 'Edit page');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>