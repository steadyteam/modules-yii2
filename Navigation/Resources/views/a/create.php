<?php

use Steady\Engine\SW;

$this->title = SW::t('admin/page', 'Create page');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>