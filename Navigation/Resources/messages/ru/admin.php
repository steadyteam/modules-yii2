<?php
return [
    'Navigation' => 'Меню',
    'Create navigation' => 'Создать меню',
    'Edit navigation' => 'Редактировать меню',
    'Navigation created' => 'Меню успешно создана',
    'Navigation updated' => 'Меню обновлена',
    'Navigation deleted' => 'Меню удалена',

    'Parent navigation' => 'Родительское меню',
];