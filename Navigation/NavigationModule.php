<?php

namespace Steady\Modules\Navigation;

use Steady\Admin\Components\SteadyModule;

class NavigationModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'navigation',
        'class' => self::class,
        'title' => [
            'en' => 'Navigation',
            'ru' => 'Навигация',
        ],
        'icon' => 'file',
        'order_num' => 80,
    ];

    public static $moduleConfig = [
    ];
}