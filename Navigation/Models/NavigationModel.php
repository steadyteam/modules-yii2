<?php

namespace Steady\Modules\Navigation\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\Aliasable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\SW;
use Steady\Engine\Validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property int navigation_id
 * @property string alias
 * @property boolean folder
 * @property string title
 *
 * @mixin SluggableBehavior
 */
class NavigationModel extends TreeModel implements Aliasable
{

    public function behaviors()
    {
        $array = [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'slugAttribute' => 'alias',
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => false,
            ],
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->folder = false;
        }
    }

    public static function tableName()
    {
        return 'navigation';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'navigation_id' => $migration->primaryKey(),
            'alias' => $migration->string(128)->notNull()->unique(),
            'folder' => $migration->boolean()->notNull()->defaultValue(false),
            'title' => $migration->string(128)->notNull(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['alias', SlugValidator::class],
            ['alias', 'required'],
            ['alias', 'unique'],
            ['folder', 'boolean'],
            ['folder', 'default', 'value' => false],
            ['title', 'required'],
            ['title', 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'parent_id' => SW::t('admin', 'Parent navigation'),
            'alias' => SW::t('admin', 'Alias'),
            'title' => SW::t('admin', 'Name'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return TreeModel
     * @throws \yii\db\Exception
     */
    public static function insertRoot(): TreeModel
    {
        $model = new NavigationModel();
        $model->alias = 'base';
        $model->type = 0;
        $model->folder = true;
        $model->title = 'base';
        $model->parent_id = null;
        $model->saveEx();
        return $model;
    }
}