<?php

use Steady\Modules\Shop\Models\OrderModel;

return [
    'params' => [
        'cart' => [
            'cookieKey' => 'cart',
            'cookieExpire' => 3600 * 24 * 365,
        ],
        'order' => [
            'states' => [
                OrderModel::STATE_NEW => 'Новый',
                OrderModel::STATE_PROGRESS => 'В ожидании',
                OrderModel::STATE_PROCESSED => 'Обрабатывается',
                OrderModel::STATE_PAID => 'Оплачен',
                OrderModel::STATE_DELIVERED => 'Доставлен',
                OrderModel::STATE_DONE => 'Завершен',
            ],
        ],
    ],
];