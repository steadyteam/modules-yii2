<?php

namespace Steady\Modules\Shop\Database;

use Steady\Modules\Shop\Models\OrderModel;

class ShopDataBase
{
    /**
     * @param int $userId
     * @return OrderModel[]|null
     */
    public static function getOrdersByUser(int $userId)
    {
        return OrderModel::find()
            ->where(['user_id' => $userId])
            ->all();
    }
}