$(window).load(function () {
    var $count = $('.js_cart_count');

    $('.js_cart_btn').on('click', function (e) {
        var $el = $(this);
        e.preventDefault();
        $.ajax({
            url: $el.attr('href'),
            type: 'GET',
            success: function (resp) {
                if (resp.hasOwnProperty('success') && resp.success === true) {
                    $count.text(parseInt($count.text()) + parseInt($el.data('quantity')));
                    var newText = $el.data('new-text');
                    var newClass = $el.data('new-class');
                    var newUrl = $el.data('new-url');
                    $el.data('new-text', $el.text());
                    $el.data('new-class', $el.attr('class'));
                    $el.data('new-url', $el.attr('href'));
                    $el.text(newText);
                    $el.removeClass().addClass(newClass);
                    $el.attr('href', newUrl);
                }
            }
        });
    });

    $('.js_cart_set_quantity').on('click', function (e) {
        var $el = $(this);
        var $quantity = $('.js_cart_quantity_' + $el.data('item-id'));
        $.ajax({
            url: $el.data('url'),
            type: 'GET',
            success: function (resp) {
                if (resp.hasOwnProperty('success') && resp.hasOwnProperty('quantity') && resp.success === true) {
                    $count.text(parseInt($count.text()) + resp.quantity - parseInt($quantity.val()));
                    $quantity.val(resp.quantity);
                }
            }
        });
    });

});