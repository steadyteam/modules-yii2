<?php

namespace Steady\Modules\Shop\Api;

use Steady\Engine\Base\Api;
use Steady\Modules\Shop\ShopModule;

abstract class CommonService extends Api
{
    public $moduleName = ShopModule::class;
}