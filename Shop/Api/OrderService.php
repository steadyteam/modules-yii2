<?php

namespace Steady\Modules\Shop\Api;

use Steady\Modules\Shop\Components\OrderManager;
use Steady\Modules\Shop\Database\ShopDataBase;
use Steady\Modules\Shop\Models\OrderModel;

/**
 * @property OrderManager manager
 * @method OrderObject get($order_id)
 */
class OrderService extends CommonService
{
    public $modelName = OrderModel::class;

    public $objectName = OrderObject::class;

    /**
     * @var OrderManager
     */
    private $_orderManager;

    /**
     * @return OrderManager
     */
    public function getManager()
    {
        if (!$this->_orderManager) {
            $this->_orderManager = new OrderManager();
        }
        return $this->_orderManager;
    }

    /**
     * @param $userId
     * @return OrderObject[]|null
     */
    public function getFromUser($userId)
    {
        $orderModels = ShopDataBase::getOrdersByUser($userId);

        /** @var OrderObject[] $orderObjects */
        $orderObjects = $this->createObjectsFromModels($orderModels);

        return $orderObjects;
    }
}