<?php

namespace Steady\Modules\Shop\Api;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\Modules\User\Models\UserModel;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Api\GoodsObject;
use Steady\Modules\Catalog\Base\GoodsItem;
use Steady\Modules\Catalog\Models\GoodsModel;
use Steady\Modules\Shop\Models\OrderModel;
use yii\helpers\Url;

/**
 * @property OrderService api
 * @property OrderModel model
 *
 * @property UserModel buyer
 * @property GoodsObject[] goodsArray
 * @property string stateText
 */
class OrderObject extends ApiObject
{
    public $modelName = OrderModel::class;

    public $order_id;
    public $user_id;
    public $state;
    public $count;
    public $amount;
    public $username;
    public $email;
    public $phone;
    public $discount;
    public $delivery_type;
    public $payment_type;
    public $comment;
    public $created_at;

    /**
     * @var UserModel
     */
    private $_buyer;

    /**
     * @var GoodsObject[]
     */
    private $_goodsItems;

    /**
     * @return array|null|UserModel
     */
    public function getBuyer()
    {
        if (!$this->_buyer) {
            $this->_buyer = $this->model->buyer();
        }
        return $this->_buyer;
    }

    /**
     * @return GoodsObject[]
     */
    public function getGoodsItems()
    {
        if (!$this->_goodsItems) {
            $data = $this->model->goods();
            /** @var GoodsModel $goodsModel */
            foreach ($data['goodsArray'] as $goodsId => $goodsModel) {
                $quantity = $data['assigns'][$goodsId]['quantity'];
                $sum = $data['assigns'][$goodsId]['sum'];
                /** @var GoodsObject $goodsObject */
                $goodsObject = $this->api->createObject($goodsModel, GoodsObject::class);
                $goodsItem = new GoodsItem($goodsId, $quantity, $goodsObject, $sum);

                $this->_goodsItems[$goodsId] = $goodsItem;
            }
        }
        return $this->_goodsItems;
    }

    /**
     * @return string|null
     */
    public function getStateText()
    {
        return SW::getModuleParams('shop')['order']['states'][$this->state];
    }

    /**
     * @name bool $scheme
     * @return string
     */
    public function url($scheme = false)
    {
        return Url::to(['/shop/order', 'id' => $this->order_id], $scheme);
    }
}