<?php

namespace Steady\Modules\Shop\Api;

use Steady\Engine\SW;
use Steady\Modules\Shop\Components\Cart;
use Steady\Modules\Shop\Storages\CartCookieStorage;
use Steady\Modules\Shop\Storages\CartDatabaseStorage;

class CartService extends CommonService
{
    /**
     * @var Cart
     */
    private $_cart;

    /**
     * @name $user
     * @return Cart
     * @throws \Exception
     */
    public function get($user = null): Cart
    {
        if (!$this->_cart) {
            $cartParams = SW::getModuleParams('shop')['cart'];

            if (SW::$app->user->isGuest) {
                $storage = new CartCookieStorage($cartParams['cookieKey'], $cartParams['cookieExpire']);
            } else {
                $storage = new CartDatabaseStorage();
            }
            $this->_cart = new Cart($storage);
        }
        return $this->_cart;
    }
}