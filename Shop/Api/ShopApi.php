<?php

namespace Steady\Modules\Shop\Api;

use Steady\Engine\Base\Api;

/**
 * @property CartService cart
 * @property OrderService order
 */
class ShopApi extends Api
{
    /**
     * @var CartService
     */
    private $_cartService;

    /**
     * @var OrderService
     */
    private $_orderService;

    /**
     * @return CartService
     */
    public function getCart(): CartService
    {
        if (!$this->_cartService) {
            $this->_cartService = new CartService();
        }
        return $this->_cartService;
    }

    /**
     * @return OrderService
     */
    public function getOrder(): OrderService
    {
        if (!$this->_orderService) {
            $this->_orderService = new OrderService();
        }
        return $this->_orderService;
    }
}