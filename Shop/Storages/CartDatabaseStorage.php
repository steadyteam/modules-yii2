<?php

namespace Steady\Modules\Shop\Storages;

use Steady\Engine\SW;
use Steady\Modules\Catalog\Api\GoodsObject;
use Steady\Modules\Catalog\Base\GoodsInterface;
use Steady\Modules\Catalog\Base\GoodsListInterface;
use Steady\Modules\Catalog\Base\StorageInterface;
use Steady\Modules\Catalog\Models\GoodsModel;
use Steady\Modules\Shop\Models\CartAssignModel;
use Steady\Modules\Shop\Models\CartModel;
use yii\helpers\ArrayHelper;

class CartDatabaseStorage implements StorageInterface
{
    /**
     * @var CartModel
     */
    public $cartModel;

    /**
     * CartDatabaseStorage constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        if (SW::$app->user->isGuest) {
            throw new \Exception('User Not Identity');
        }
        $this->cartModel = CartModel::find()->where(['user_id' => SW::$app->user->getId()])->one();
        if (!$this->cartModel) {
            $cartModel = new CartModel();
            $cartModel->user_id = SW::$app->user->getId();
            $cartModel->count = 0;
            $cartModel->amount = 0;
            $cartModel->save();
            $this->cartModel = $cartModel;
        }
    }

    /**
     * @name GoodsListInterface $goodsList
     * @return bool
     */
    public function save(GoodsListInterface $goodsList): bool
    {
        /** @var GoodsInterface[] $goodsArray */
        $goodsArray = $goodsList->fetch();

        /** @var CartAssignModel[] $cartAssigns */
        $cartAssigns = CartAssignModel::find()
            ->where(['cart_id' => $this->cartModel->cart_id])
            ->indexBy('goods_id')
            ->all();


        if (!empty($goodsArray)) {
            foreach ($goodsArray as $goods) {
                $exists = isset($cartAssigns[$goods->getId()]);
                if (!$exists) {
                    CartAssignModel::add($this->cartModel->cart_id, $goods->getId(),
                        $goodsList->quantity($goods->getUniqueId()));
                }
            }
        }

        if (!empty($cartAssigns)) {
            foreach ($cartAssigns as $cartAssign) {
                $uniqueId = GoodsModel::getClassAlias() . '_' . $cartAssign->goods_id;
                if ($goodsList->get($uniqueId)) {
                    if ($goodsList->quantity($uniqueId) != $cartAssign->quantity) {
                        $cartAssign->quantity = $goodsList->quantity($uniqueId);
                        $cartAssign->save();
                    }
                } else {
                    try {
                        $cartAssign->delete();
                    } catch (\Throwable $e) {
                    }
                }
            }
        }

        return true;
    }

    /**
     * @name GoodsListInterface $goodsList
     * @return bool
     */
    public function load(GoodsListInterface $goodsList): bool
    {
        $assigns = CartAssignModel::find()
            ->where(['cart_id' => $this->cartModel->cart_id])
            ->asArray()
            ->indexBy('goods_id')
            ->all();

        if (empty($assigns)) {
            return false;
        }

        $goodsModels = GoodsModel::find()
            ->where(['goods_id' => ArrayHelper::getColumn($assigns, 'goods_id')])
            ->all();

        /** @var GoodsObject[] $goodsArray */
        $goodsArray = SW::$app->api->catalog->createObjectsFromModels($goodsModels, GoodsObject::class);

        foreach ($goodsArray as $goods) {
            $goodsList->put($goods, $assigns[$goods->goods_id]['quantity'], false, false);
        }

        $this->synch($goodsList);

        return true;
    }

    /**
     * @name GoodsListInterface $goodsList
     * @return bool
     */
    public function synch(GoodsListInterface $goodsList)
    {
        $cookieStorage = new CartCookieStorage(SW::getModuleParams('shop')['cart']['cookieKey']);
        $cookieStorage->synch($goodsList);

        return true;
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function clear()
    {
        $cartAssigns = CartAssignModel::find()
            ->where(['cart_id' => $this->cartModel->cart_id])
            ->all();

        foreach ($cartAssigns as $cartAssign) {
            $cartAssign->delete();
        }

        $this->cartModel->delete();

        return true;
    }
}