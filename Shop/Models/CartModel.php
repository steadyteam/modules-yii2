<?php

namespace Steady\Modules\Shop\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;

/**
 * @property int cart_id
 * @property int user_id
 * @property float count
 * @property float amount
 * @property int discount
 * @property int coupon
 * @property string|array data
 */
class CartModel extends AdvancedModel
{
    public function behaviors()
    {
        $array = [
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    public static function tableName()
    {
        return 'shop_cart';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'cart_id' => $migration->primaryKey(),
            'user_id' => $migration->integer(11)->notNull(),
            'count' => $migration->float(5)->notNull(),
            'amount' => $migration->float(11)->notNull(),
            'discount' => $migration->integer(5)->null(),
            'coupon' => $migration->integer(5)->null(),
            'data' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['user_id', 'integer'],
            ['user_id', 'required'],
            ['count', 'number'],
            ['count', 'required'],
            ['amount', 'number'],
            ['amount', 'required'],
            ['discount', 'integer', 'max' => 99],
            ['coupon', 'integer'],
            ['data', 'safe'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'user_id' => SW::t('admin/shop', 'UserId'),
            'count' => SW::t('admin/shop', 'Count'),
            'amount' => SW::t('admin/shop', 'Amount'),
            'discount' => SW::t('admin/shop', 'Discount'),
            'coupon' => SW::t('admin/shop', 'Coupon'),
            'data' => SW::t('admin/shop', 'Data'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    public static function create()
    {

    }
}