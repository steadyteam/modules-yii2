<?php

namespace Steady\Modules\Shop\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\helpers\ArrayHelper;

/**
 * @property int order_assign_id
 * @property int order_id
 * @property int goods_id
 * @property int offer_id
 * @property float quantity
 * @property float sum
 */
class OrderAssignModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'shop_order_assign';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'order_assign_id' => $migration->primaryKey(),
            'order_id' => $migration->integer(11)->notNull(),
            'goods_id' => $migration->integer(11)->notNull(),
            'offer_id' => $migration->integer(11)/*->notNull()*/
            ->null(),
            'quantity' => $migration->float(11)->notNull(),
            'sum' => $migration->float(11)->notNull(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['order_id', 'integer'],
            ['order_id', 'required'],
            ['goods_id', 'integer'],
            ['goods_id', 'required'],
            ['offer_id', 'integer'],
            //['offer_id', 'required'],
            ['quantity', 'number'],
            ['quantity', 'required'],
            ['sum', 'number'],
            ['sum', 'required'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getGoodsRelation()
    {
        return $this->hasMany(GoodsModel::class, ['goods_id' => 'goods_id']);
    }

    /**
     * @param int $orderId
     * @param int $goodsId
     * @param int|null $offer_id
     * @param string $quantity
     * @param string $sum
     * @return OrderAssignModel
     * @throws \yii\db\Exception
     */
    public static function add(int $orderId, int $goodsId, ?int $offer_id, string $quantity, string $sum): OrderAssignModel
    {
        $model = new OrderAssignModel();
        $model->order_id = $orderId;
        $model->goods_id = $goodsId;
        $model->offer_id = $goodsId;
        $model->quantity = $quantity;
        $model->sum = $sum;
        $model->saveEx();
        return $model;
    }

    /**
     * @name int $orderId
     * @name int $goodsId
     * @return null|OrderAssignModel
     */
    public static function getAssign(int $orderId, int $goodsId): ?OrderAssignModel
    {
        return OrderAssignModel::findOne(['order_id' => $orderId, 'goods_id' => $goodsId]);
    }
}