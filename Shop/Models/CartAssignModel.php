<?php

namespace Steady\Modules\Shop\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\helpers\ArrayHelper;

/**
 * @property int cart_assign_id
 * @property int cart_id
 * @property int goods_id
 * @property float quantity
 */
class CartAssignModel extends AdvancedModel
{
    public static function tableName()
    {
        return 'shop_cart_assign';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'cart_assign_id' => $migration->primaryKey(),
            'cart_id' => $migration->integer(11)->notNull(),
            'goods_id' => $migration->integer(11)->notNull(),
            'quantity' => $migration->float(11)->notNull(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['cart_id', 'integer'],
            ['cart_id', 'required'],
            ['goods_id', 'integer'],
            ['goods_id', 'required'],
            ['quantity', 'double'],
            ['quantity', 'required'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsRelation()
    {
        return $this->hasOne(GoodsModel::class, ['goods_id' => 'goods_id']);
    }

    /**
     * @name int $cartId
     * @name int $goodsId
     * @name string $quantity
     * @return CartAssignModel
     */
    public static function add(int $cartId, int $goodsId, string $quantity): CartAssignModel
    {
        $model = new CartAssignModel();
        $model->cart_id = $cartId;
        $model->goods_id = $goodsId;
        $model->quantity = $quantity;
        $model->save();
        return $model;
    }

    /**
     * @name int $cartId
     * @name int $goodsId
     * @return null|CartAssignModel
     */
    public static function getAssign(int $cartId, int $goodsId): ?CartAssignModel
    {
        return CartAssignModel::findOne(['cart_id' => $cartId, 'goods_id' => $goodsId]);
    }
}