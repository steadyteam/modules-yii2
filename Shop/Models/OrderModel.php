<?php

namespace Steady\Modules\Shop\Models;

use Steady\Engine\Base\Migration;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Modules\User\Models\UserModel;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\helpers\ArrayHelper;

/**
 * @property int order_id
 * @property int user_id
 * @property int state
 * @property float count
 * @property float amount
 * @property string username
 * @property string email
 * @property string phone
 * @property int discount
 * @property int delivery_type
 * @property int payment_type
 * @property string comment
 * @property string security_key
 * @property string|array data
 *
 * @property UserModel userRelation
 * @property OrderAssignModel[] assignRelation
 */
class OrderModel extends AdvancedModel
{
    const STATE_NEW = 0;
    const STATE_PROGRESS = 5;
    const STATE_PROCESSED = 10;
    const STATE_PAID = 15;
    const STATE_DELIVERED = 20;
    const STATE_DONE = 25;

    public function behaviors()
    {
        $array = [
        ];

        return ArrayHelper::merge($array, parent::behaviors());
    }

    /**
     * @throws \yii\base\Exception
     */
    public function init()
    {
        parent::init();

        if ($this->isNewRecord) {
            $this->state = self::STATE_NEW;
            $this->security_key = self::generateSecurityKey();
        }
    }

    public static function tableName()
    {
        return 'shop_order';
    }

    public static function migrationUp(Migration $migration, array $columns = []): bool
    {
        $columns = [
            'order_id' => $migration->primaryKey(),
            'user_id' => $migration->integer(11)->null(),
            'state' => $migration->integer(3)->notNull(),
            'count' => $migration->float(11)->notNull(),
            'amount' => $migration->float(11)->notNull(),
            'username' => $migration->string(128)->notNull(),
            'email' => $migration->integer(128)->null(),
            'phone' => $migration->integer(128)->null(),
            'discount' => $migration->integer(11)->null(),
            'delivery_type' => $migration->integer(3)->null(),
            'payment_type' => $migration->integer(3)->null(),
            'comment' => $migration->text()->null(),
            'security_key' => $migration->string(64)->notNull(),
            'data' => $migration->text(),
        ];

        $success = parent::migrationUp($migration, $columns);

        return $success;
    }

    public function rules()
    {
        $array = [
            ['user_id', 'integer'],
            //['user_id', 'required'],
            ['state', 'integer'],
            ['state', 'required'],
            ['count', 'number'],
            ['count', 'required'],
            ['amount', 'number'],
            ['amount', 'required'],
            ['username', 'string'],
            ['username', 'required'],
            ['email', 'string'],
            ['phone', 'string'],
            ['discount', 'integer', 'max' => 99],
            ['delivery_type', 'integer'],
            ['payment_type', 'integer'],
            ['comment', 'string'],
            ['security_key', 'string'],
            ['security_key', 'required'],
            ['data', 'safe'],
            [['username', 'email', 'phone', 'comment'], 'trim'],
        ];

        return ArrayHelper::merge($array, parent::rules());
    }

    public function attributeLabels()
    {
        $array = [
            'user_id' => SW::t('admin/shop', 'UserId'),
            'state' => SW::t('admin/shop', 'State'),
            'count' => SW::t('admin/shop', 'Count'),
            'amount' => SW::t('admin/shop', 'Amount'),
            'discount' => SW::t('admin/shop', 'Discount'),
            'data' => SW::t('admin/shop', 'Data'),
        ];

        return ArrayHelper::merge($array, parent::attributeLabels());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getUserRelation()
    {
        return $this->hasOne(UserModel::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getAssignRelation()
    {
        return $this->hasMany(OrderAssignModel::class, ['order_id' => 'order_id']);
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return UserModel|null
     */
    public function buyer(): ?UserModel
    {
        return $this->userRelation;
    }

    /**
     * @return array
     */
    public function goods()
    {
        $assigns = $this->getAssignRelation()
            ->indexBy('goods_id')
            ->asArray()
            ->all();

        $goodsArray = GoodsModel::find()
            ->where(['goods_id' => ArrayHelper::getColumn($assigns, 'goods_id')])
            ->indexBy('goods_id')
            ->all();

        return [
            'assigns' => $assigns,
            'goodsArray' => $goodsArray,
        ];
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    private function generateSecurityKey(): string
    {
        return SW::$app->security->generateRandomString(64);
    }
}