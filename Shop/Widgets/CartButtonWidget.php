<?php

namespace Steady\Modules\Shop\Widgets;

use Steady\Engine\SW;
use Steady\Modules\Catalog\Base\GoodsInterface;
use Steady\Modules\Shop\Components\Cart;
use yii\base\Widget;
use yii\helpers\Url;

class CartButtonWidget extends Widget
{
    /**
     * @var Cart
     */
    public $cart;

    /**
     * @var GoodsInterface
     */
    public $item;

    /**
     * @var string
     */
    public $actions = ['add', 'remove'];

    /**
     * @var array
     */
    public $classCss = [];

    /**
     * @var array
     */
    public $text = ['В корзину', 'В корзине'];

    /**
     * @var bool
     */
    public $put;

    /**
     * @throws \Exception
     */
    public function init()
    {
        parent::init();

        $this->cart = SW::$app->api->shop->cart->get();
        $this->put = $this->cart->get($this->item->getUniqueId());
    }

    /**
     * @return string|void
     */
    public function run()
    {
        $addUrl = Url::to(['/shop-action/add-to-cart', 'id' => $this->item->getId()]);
        $removeUrl = Url::to(['/shop-action/remove-from-cart', 'id' => $this->item->getId()]);

        $secondUrl = null;
        if ($this->actions[1] === 'remove') {
            $secondUrl = $removeUrl;
        } else if ($this->actions[1] === 'cart') {
            $secondUrl = Url::to(['/cart']);
        } else if ($this->actions[1] === 'popup') {
            $secondUrl = Url::to(['/cart']);
        }

        if ($this->put) {
            $url = $secondUrl;
            $newUrl = $addUrl;
        } else {
            $url = $addUrl;
            $newUrl = $secondUrl;
        }

        $class = $this->getValue($this->classCss, $this->put);
        $text = $this->getValue($this->text, $this->put);
        $action = $this->getValue($this->actions, $this->put);
        $newAction = $this->getValue($this->actions, !$this->put);
        $newClass = $this->getValue($this->classCss, !$this->put);
        $newText = $this->getValue($this->text, !$this->put);
        $changeQuantity = $this->put ? -$this->cart->quantity($this->item->getUniqueId()) : 1;
        $quantity = $this->put ? $this->cart->quantity($this->item->getUniqueId()) : 1;

        echo "
            <a href='$url' class='$class' data-action='$action' data-new-action='$newAction' data-new-text='$newText' 
                data-new-class='$newClass' data-new-url='$newUrl' data-change-quantity='$changeQuantity' 
                data-quantity='$quantity'>
                $text
            </a>
        ";
    }

    /**
     * @name $value
     * @name $condition
     * @return mixed
     */
    private function getValue($value, $condition)
    {
        return is_array($value) ? $value[$condition ? 1 : 0] : $value;
    }
}