<?php

namespace Steady\Modules\Shop\Components;

use Steady\Engine\Base\Controller;
use Steady\Engine\Helpers\Mail;
use Steady\Engine\SW;
use Steady\Modules\Shop\Api\OrderObject;
use Steady\Modules\Shop\Forms\OrderForm;
use yii\web\Response;

class ShopActionController extends Controller
{
    /**
     * @var array
     */
    protected $orderEmailParams = [];

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        // TODO: Временное рещение, разобраться c CSRF
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * @name $id
     * @return array|Response
     * @throws \Exception
     */
    public function actionAddToCart($id)
    {
        $item = SW::$app->api->catalog->category->item($id);
        $cart = SW::$app->api->shop->cart->get();

        $cart->put($item);

        if (SW::$app->request->isAjax) {
            SW::$app->response->format = Response::FORMAT_JSON;
            return [
                'success' => true,
                'quantity' => $cart->quantity($item->getUniqueId()),
            ];
        }

        return $this->redirect(['/cart']);
    }

    /**
     * @name $id
     * @return array|Response
     * @throws \Exception
     */
    public function actionRemoveFromCart($id)
    {
        $item = SW::$app->api->catalog->category->item($id);
        $cart = SW::$app->api->shop->cart->get();

        $cart->remove($item->getUniqueId());

        if (SW::$app->request->isAjax) {
            SW::$app->response->format = Response::FORMAT_JSON;
            return [
                'success' => true,
                'quantity' => 0,
            ];
        }

        return $this->redirect(['/cart']);
    }

    /**
     * @name $id
     * @return array
     * @throws \Exception
     */
    public function actionPlus($id)
    {
        $item = SW::$app->api->catalog->category->item($id);
        $cart = SW::$app->api->shop->cart->get();

        $cart->plus($item->getUniqueId());

        SW::$app->response->format = Response::FORMAT_JSON;
        return [
            'success' => true,
            'quantity' => $cart->quantity($item->getUniqueId()),
        ];
    }

    /**
     * @name $id
     * @return array
     * @throws \Exception
     */
    public function actionMinus($id)
    {
        $item = SW::$app->api->catalog->category->item($id);
        $cart = SW::$app->api->shop->cart->get();

        $cart->minus($item->getUniqueId());

        SW::$app->response->format = Response::FORMAT_JSON;
        return [
            'success' => true,
            'quantity' => $cart->quantity($item->getUniqueId()),
        ];
    }

    /**
     * @return array|Response
     * @throws \Exception
     */
    public function actionCreateOrder()
    {
        $orderForm = new OrderForm();

        if ($orderForm->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                return $this->ajaxValidate($orderForm);
            } else if ($orderForm->validate()) {
                try {
                    $order = SW::$app->api->shop->order->manager->create($orderForm);
                    $this->sendOrderToEmails($order);
                    return $this->redirect(['/order', 'id' => $order->order_id, 'type' => 'new',
                        'key' => $order->model->security_key]);
                } catch (\Exception $e) {
                    return $this->redirect(['/cart', 'error' => $e->getMessage()]);
                }
            }
        }
        return $this->redirect(['/cart', 'error' => 'unknown']);
    }

    /**
     * @param OrderObject $order
     * @return bool
     * @throws \Exception
     */
    protected function sendOrderToEmails($order)
    {
        $this->makeOrderEmailParams();

        $this->sendOrderToShopEmail($order);
        $this->sendOrderToBuyerEmail($order);

        return true;
    }

    protected function makeOrderEmailParams()
    {
        $this->orderEmailParams = [
            'shop' => [
                'view' => '@modules/Shop/Resources/mail/order_to_shop.twig',
                'to' => SW::$app->api->text->get('email-order'),
                'subject' => 'Поступил заказ товара с сайта',
            ],
            'buyer' => [
                'view' => '@modules/Shop/Resources/mail/order_to_buyer.twig',
                'from' => SW::$app->api->text->get('email-order'),
                'fromName' => SW::$app->api->text->get('company-name'),
                'subject' => 'Ваш заказ принят',
            ],
        ];
    }

    /**
     * @param OrderObject $order
     * @return bool
     * @throws \Exception
     */
    protected function sendOrderToShopEmail($order)
    {
        $body = $this->renderFile($this->orderEmailParams['shop']['view'], [
            'order' => $order,
        ]);
        $toShop = Mail::newInstance()
            ->setFrom($order->email)
            ->setFromName($order->username)
            ->setTo($this->orderEmailParams['shop']['to'])
            ->setSubject($this->orderEmailParams['shop']['subject'])
            ->setTextBody($body)
            ->send();

        return $toShop;
    }

    /**
     * @param OrderObject $order
     * @return bool
     * @throws \Exception
     */
    protected function sendOrderToBuyerEmail($order)
    {
        $body = $this->renderFile($this->orderEmailParams['buyer']['view'], [
            'order' => $order,
        ]);

        $toClient = Mail::newInstance()
            ->setFrom($this->orderEmailParams['buyer']['from'])
            ->setFromName($this->orderEmailParams['buyer']['fromName'])
            ->setTo($order->email)
            ->setSubject($this->orderEmailParams['buyer']['subject'])
            ->setTextBody($body)
            ->send();

        return $toClient;
    }
}