<?php

namespace Steady\Modules\Shop\Components;

use Steady\Modules\Catalog\Base\GoodsList;

class Cart extends GoodsList
{
    /**
     * Итоговая стоимость корзины
     * @return float|int
     * @throws \Exception
     */
    public function totalAmount()
    {
        $total = $this->amount() - $this->discount();

        return $total;
    }

    /**
     * Сумма скидки на товары в корзине
     * @return int
     */
    public function discount()
    {
        return 0;
    }
}