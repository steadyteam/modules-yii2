<?php

namespace Steady\Modules\Shop\Components;

use Steady\Modules\Catalog\Base\GoodsListInterface;

interface CartInterface extends GoodsListInterface
{
}