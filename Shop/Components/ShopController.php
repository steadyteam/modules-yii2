<?php

namespace Steady\Modules\Shop\Components;

use Steady\Engine\Modules\Page\Components\PageController;
use Steady\Engine\SW;
use Steady\Modules\Shop\Forms\OrderForm;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class ShopController extends PageController
{
    /**
     * @var array
     */
    protected $params = [];

    /**
     * @var string
     */
    protected $cartView = 'shop/cart';

    /**
     * @var string
     */
    protected $orderView = 'shop/order';

    /**
     * @var string
     */
    protected $ordersView = 'shop/orders';

    /**
     * @return string
     * @throws \Exception
     */
    public function actionCart()
    {
        $cart = SW::$app->api->shop->cart->get();

        $orderForm = new OrderForm();

        $this->params = [
            'cart' => $cart,
            'goodsArray' => $cart->fetch(),
            'amount' => $cart->amount(),
            'orderForm' => $orderForm,
        ];

        return $this->render($this->cartView, $this->params);
    }

    /**
     * @param $id
     * @param string $type
     * @param null $key
     * @return string
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionOrder($id, $type = 'info', $key = null)
    {
        $order = SW::$app->api->shop->order->get($id);
        if (!$order) {
            throw new NotFoundHttpException('Order not found.');
        }
        if (!(($order->user_id && $order->user_id === SW::$app->user->getId()) ||
            ($order->model->security_key === $key))) {
            throw new ForbiddenHttpException('Access denied.');
        }

        $this->params = [
            'type' => $type,
            'order' => $order,
        ];

        return $this->render($this->orderView, $this->params);
    }

    /**
     * @param $userId
     * @return string
     */
    public function actionOrders()
    {
        if (SW::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }

        $user = SW::$app->user->identity;
        $orders = SW::$app->api->shop->order->getFromUser($user->getId());

        $this->params = [
            'user' => $user,
            'orders' => $orders,
        ];

        return $this->render($this->ordersView, $this->params);
    }

    /**
     * @return string
     */
    protected function getPageSlug()
    {
        return SW::$app->controller->route;
    }
}