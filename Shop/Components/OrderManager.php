<?php

namespace Steady\Modules\Shop\Components;

use Steady\Engine\SW;
use Steady\Modules\Shop\Api\OrderObject;
use Steady\Modules\Shop\Forms\OrderForm;
use Steady\Modules\Shop\Models\OrderAssignModel;
use Steady\Modules\Shop\Models\OrderModel;
use yii\base\InvalidArgumentException;

class OrderManager
{
    /**
     * @param OrderForm $orderForm
     * @return OrderObject
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function create(OrderForm $orderForm): OrderObject
    {
        $cart = SW::$app->api->shop->cart->get();
        $goodsItems = $cart->fetch();

        if (!$goodsItems || count($goodsItems) <= 0) {
            throw new InvalidArgumentException('GoodsItems not found in cart');
        }
        $transaction = $transaction = SW::$app->db->beginTransaction();
        try {
            $orderModel = new OrderModel();
            $orderModel->user_id = SW::$app->user->getId();
            $orderModel->state = OrderModel::STATE_NEW;
            $orderModel->count = $cart->count();
            $orderModel->amount = $cart->amount();
            $orderModel->username = $orderForm->name;
            $orderModel->email = $orderForm->email;
            $orderModel->phone = $orderForm->phone;
            $orderModel->discount = 0;
            $orderModel->delivery_type = $orderForm->deliveryType;
            $orderModel->payment_type = $orderForm->paymentType;
            $orderModel->saveEx();

            foreach ($goodsItems as $goods) {
                $sum = $goods->quantity * $goods->getPrice();
                OrderAssignModel::add($orderModel->order_id, $goods->goodsId, null, $goods->quantity, $sum);
            }

            /** @var OrderObject $order */
            $order = SW::$app->api->shop->order->createObject($orderModel);

            $cart->clear();

            $transaction->commit();
            return $order;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}