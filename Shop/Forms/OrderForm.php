<?php

namespace Steady\Modules\Shop\Forms;

use Steady\Engine\Base\Form;
use Steady\Engine\SW;
use Steady\Engine\Validators\PhoneValidator;

class OrderForm extends Form
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var int
     */
    public $deliveryType;

    /**
     * @var int
     */
    public $paymentType;

    public function rules()
    {
        return [
            ['name', 'string'],
            ['name', 'required'],
            ['email', 'email'],
            ['email', 'required'],
            ['phone', PhoneValidator::class],
            //['phone', 'required'],
            ['city', 'string'],
            ['city', 'required'],
            ['comment', 'string'],
            ['deliveryType', 'integer'],
            ['paymentType', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => SW::t('admin', 'Имя'),
            'email' => SW::t('admin', 'Email'),
            'phone' => SW::t('admin', 'Телефон'),
            'city' => SW::t('admin', 'Город'),
            'comment' => SW::t('admin', 'Комментарий к заказу'),
        ];
    }
}