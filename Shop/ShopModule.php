<?php

namespace Steady\Modules\Shop;

use Steady\Admin\Components\SteadyModule;

class ShopModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'shop',
        'class' => self::class,
        'title' => [
            'en' => 'Shop',
            'ru' => 'Магазин',
        ],
        'icon' => 'list-alt',
        'order_num' => 70,
    ];
}