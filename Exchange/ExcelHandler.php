<?php

namespace Steady\Admin\Handlers;

use moonland\phpexcel\Excel;
use Steady\Engine\Base\Model;
use Steady\Engine\Models\SeoTextModel;
use Steady\Engine\Modules\Page\Models\PageModel;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\base\BaseObject;
use yii\db\Exception;

class ExcelHandler extends BaseObject
{
    /**
     * @var string
     */
    public $fileName;

    /**
     * Import data
     * @var array
     */
    public $data;

    /**
     * Import settings
     * @var array
     */
    public $functions = [];

    /**
     * @return array
     * @throws Exception
     * @throws \Exception
     */
    public function import()
    {
        $transaction = SW::$app->db->beginTransaction();
        try {
            $this->data = Excel::widget([
                'mode' => 'import',
                'fileName' => SW::getAlias('@uploads/import/') . $this->fileName,
                'setFirstRecordAsKeys' => true,
                'setIndexSheetByName' => true,
                //'getOnlySheet' => 'Categories',
            ]);

            $this->importManufacturer();
            $this->importCategories();
            $this->importProducts();

            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->functions;
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function importCategories()
    {
        if (!$this->functions['categories']['status']) return false;

        $base = CategoryModel::findOne(['category_id' => 1]);
        if (!$base) {
            $base = new CategoryModel();
            $base->title = 'Базовая';
            $base->slug = 'base';
            $base->makeRoot();
            $base->save();
        }

        foreach ($this->data['Categories'] as $cat) {
            if (!$cat['name']) continue;

            $category = CategoryModel::find()->where(['title' => $cat['name']])->one();
            if (!$category) $category = new CategoryModel();
            $category->title = $cat['name'];
            $category->slug = end(explode('/', $cat['url']));

            if (!$cat['parent_id']) {
                $cat['parent_id'] = 'Базовая';
            }
            $parent = CategoryModel::find()->where(['title' => $cat['parent_id']])->one();
            if (!$parent) throw new Exception('Parent category "' . $cat['parent_id'] . '" not found');

            $category->parent_id = $parent->category_id;
            try {
                $saved = $category->save();
            } catch (\Exception $e) {
                throw new Exception('Category "' . $category->title . '" can not move');
            }

            if ($saved) {
                throw new Exception('Category "' . $category->title . '" not saved: ', $category->getErrors());
            }

            $this->attachSeo($category, $cat);

            $this->functions['categories']['count']++;
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function importManufacturer()
    {
        if (!$this->functions['manufacturer']['status']) return false;

        foreach ($this->data['Manufacturer'] as $mfr) {
            if (!$mfr['name']) continue;

            $slug = "partners/" . end(explode('/', $mfr['url']));

            $page = PageModel::find()->where(['slug' => $slug])->one();
            if (!$page) $page = new PageModel();
            $page->layout_id = 5;
            $page->parent_id = 3;
            $page->type = 1;
            $page->slug = $slug;
            $page->title = $mfr['name'];
            $page->preview = $mfr['image_url'];
            $page->content = $mfr['description'];
            $saved = $page->save();
            if ($saved) {
                throw new Exception('Page "' . $page->title . '" not saved: ', $page->getErrors());
            };

            $this->attachSeo($page, $mfr);

            $this->functions['manufacturer']['count']++;
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function importProducts()
    {
        if (!$this->functions['products']['status']) return false;

        foreach ($this->data['Product'] as $prod) {
            if (!$prod['name']) continue;

            $category = CategoryModel::find()->where(['title' => $prod['categories_id']])->one();
            if (!$category) {
                throw new Exception('Category "' . $prod['categories_id'] . '" not found');
            }
            $slug = end(explode('/', $prod['url']));

            $goods = GoodsModel::find()->where(['slug' => $slug])->one();
            if (!$goods) $goods = new GoodsModel();
            $goods->category_id = $category->category_id;
            $goods->slug = $slug;
            $goods->status = GoodsModel::STATUS_ON;
            $goods->title = $prod['name'];
            $goods->description = $prod['product_description'];
            $goods->image = $prod['image_url'];
            $saved = $goods->save();
            if ($saved) {
                throw new Exception('Goods "' . $goods->title . '" not saved: ', $goods->getErrors());
            };

            $this->attachSeo($goods, $prod);

            $this->functions['products']['count']++;
        }

        return true;
    }

    /**
     * @name $object Model
     * @name $item array
     * @return bool
     * @throws Exception
     */
    private function attachSeo($object, $item)
    {
        if (!$this->functions['seoText']['status']) return false;

        $class = get_class($object);

        if (!$object->primaryKey) {
            throw new Exception('In "' . $class . '" not found primaryKey for SeoText: ', $object->getErrors());
        }

        $seoText = SeoTextModel::find()->where(['class' => $class, 'item_id' => $object->primaryKey])->one();
        if (!$seoText) $seoText = new SeoTextModel();
        $seoText->class = $class;
        $seoText->item_id = $object->primaryKey;
        $seoText->h1 = $item['h1'];
        $seoText->title = $item['meta_title'];
        $seoText->description = $item['meta_description'];
        $seoText->keywords = $item['meta_keywords'];
        $saved = $seoText->save();
        if ($saved) {
            $seoId = "$seoText->class item: $seoText->item_id";
            throw new Exception('SeoText "' . $seoId . '" not saved: ', $seoText->getErrors());
        };

        $this->functions['seoText']['count']++;

        return true;
    }
}