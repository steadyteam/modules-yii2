<?php

namespace Steady\Admin\Commands;

use Steady\Admin\Handlers\ExcelHandler;
use yii\console\Controller;

class ExchangeController extends Controller
{
    /**
     * @name $filename
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionImportExcel($filename)
    {
        echo "Begin importing a file...\n";

        $excelData = new ExcelHandler(['fileName' => $filename]);
        $result = $excelData->import();

        foreach ($result as $key => $item) {
            if ($item['status']) {
                echo "$key: " . $item['count'] . " items imported\n";
            }
        }

        echo "Import complete!\n";
    }
}