<?php

namespace Steady\Admin\Controllers;

use Steady\Admin\Components\ApiController;
use Steady\Admin\Handlers\ExcelHandler;

class ExchangeController extends ApiController
{
    /**
     * @name $filename
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionImportExcel($filename)
    {
        $excelData = new ExcelHandler(['fileName' => $filename]);
        $result = $excelData->import();

        foreach ($result as $key => $item) {
            if ($item['status']) {
                echo "$key: " . $item['count'] . " items imported\n";
            }
        }
    }

}
